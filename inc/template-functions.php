<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Two_Plus_O
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function two_plus_o_body_classes( $classes ) {
    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    // Adds a class of no-sidebar when there is no sidebar present.
    if ( ! is_active_sidebar( 'sidebar-1' ) ) {
        $classes[] = 'no-sidebar';
    }

    return $classes;
}
add_filter( 'body_class', 'two_plus_o_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function two_plus_o_pingback_header() {
    if ( is_singular() && pings_open() ) {
        printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
    }
}
add_action( 'wp_head', 'two_plus_o_pingback_header' );


function my_filter_plugin_updates( $value ) {
   if( isset( $value->response['wp-multi-step-checkout/wp-multi-step-checkout.php'] ) ) {        
      unset( $value->response['wp-multi-step-checkout/wp-multi-step-checkout.php'] );
    }
    return $value;
 }
 add_filter( 'site_transient_update_plugins', 'my_filter_plugin_updates' );

// Woocommerce
/**
* is_realy_woocommerce_page - Returns true if on a page which uses WooCommerce templates (cart and checkout are standard pages with shortcodes and which are also included)
*
* @access public
* @return bool
*/
function is_realy_woocommerce_page () {
    if( function_exists ( "is_woocommerce" ) && is_woocommerce()){
        return true;
    }
    $woocommerce_keys = array ( "woocommerce_shop_page_id" ,
        "woocommerce_cart_page_id" ,
        "woocommerce_checkout_page_id" ,
        "woocommerce_pay_page_id" ,
        "woocommerce_thanks_page_id" ,
        "woocommerce_myaccount_page_id" ,
        "woocommerce_edit_address_page_id" ,
        "woocommerce_view_order_page_id" ,
        "woocommerce_change_password_page_id" ,
        "woocommerce_logout_page_id" ,
        "woocommerce_lost_password_page_id" ) ;

    foreach ( $woocommerce_keys as $wc_page_id ) {
        if ( get_the_ID () == get_option ( $wc_page_id , 0 ) ) {
            return true ;
        }
    }
    return false;
}

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

function setup_woocommerce_support()
{
    add_theme_support( 'wc-product-gallery-zoom' );
    // add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );

    add_theme_support('woocommerce');
}
add_action( 'after_setup_theme', 'setup_woocommerce_support' );

add_filter( 'woocommerce_enqueue_styles', '__return_false' );


function filter_woocommerce_form_field_args( $args, $key, $value ) { 
    $args['class'][0] = 'form-group';
    $args['input_class'][0] = 'cf-input';
    $args['input_class'][1] = 'form-control';
    return $args; 
}; 
add_filter( 'woocommerce_form_field_args', 'filter_woocommerce_form_field_args', 10, 3 ); 

function filter_woocommerce_form_field( $field, $key, $args, $value ) { 

    if($key == 'billing_first_name' || $key == 'shipping_first_name'){
        $field = str_replace('<p class="form-row', '<div class="form-row"><div class="row"><div class="col-sm-6', $field);
        $field = str_replace('></span></p>', '></span></div>', $field);
    } elseif($key == 'billing_last_name' || $key == 'shipping_last_name') {
        $field = str_replace('<p class="form-row', '<div class="col-sm-6', $field);
        $field = str_replace('></span></p>', '></span></div></div></div>', $field);
    } elseif($key == 'billing_dob_date') {
        $field = str_replace('<p class="form-row', '<div class="form-row"><div class="row"><div class="col-sm-4 form-group', $field);
        $field = str_replace('<select name="', '<div class="bp-select cf-select"><select name="', $field);
        $field = str_replace('></span></p>', '></span></div></div>', $field);
    } elseif($key == 'billing_dob_month') {
        $field = str_replace('<p class="form-row', '<div class="col-sm-4 form-group', $field);
        $field = str_replace('<select name="', '<div class="bp-select cf-select"><select name="', $field);
        $field = str_replace('></span></p>', '></span></div></div>', $field);
    } elseif($key == 'billing_dob_year') {
        $field = str_replace('<p class="form-row', '<div class="col-sm-4 form-group', $field);
        $field = str_replace('<select name="', '<div class="bp-select cf-select"><select name="', $field);
        $field = str_replace('></span></p>', '></span></div></div></div></div>', $field);
    } else {

        // $field = str_replace('></span></p>', '></span></div>', $field);
        // $field = str_replace('<p class="form-row', '<div class="form-row', $field);

        // $field = str_replace('<select name="', '<div class="bp-select cf-select"><select name="', $field);
        // $field = str_replace('</select>', '</select></div>', $field);
    }

    return $field;
}; 
add_filter( 'woocommerce_form_field', 'filter_woocommerce_form_field', 999, 4 ); 

// define the woocommerce_cart_totals_coupon_label callback 
function filter_woocommerce_cart_totals_coupon_label( $sprintf, $coupon ) { 
    $sprintf = sprintf( esc_html__( '%s', 'woocommerce' ), $coupon->get_code() );
    return $sprintf; 
}; 
         
// add the filter 
add_filter( 'woocommerce_cart_totals_coupon_label', 'filter_woocommerce_cart_totals_coupon_label', 10, 2 ); 

// define the woocommerce_cart_shipping_method_full_label callback 
function filter_woocommerce_cart_shipping_method_full_label( $label, $method ) { 

    $label = str_replace(':', '', $label);
    $label = str_replace(' - ', '<br>', $label);
    $label = str_replace('span', 'strong', $label);


    // $label = str_replace('<small><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>135</span></small>', '></span></div>', $label);
    // $label = str_replace('<small><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>135</span></small>', '></span></div>', $label);

    // make filter magic happen here... 
    return $label; 
}; 
         
// add the filter 
add_filter( 'woocommerce_cart_shipping_method_full_label', 'filter_woocommerce_cart_shipping_method_full_label', 10, 2 ); 

// On cart page
add_action( 'woocommerce_cart_collaterals', 'remove_cart_totals', 9 );
function remove_cart_totals(){
    // Remove cart totals block
    remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );
    // Add back "Proceed to checkout" button (and hooks)
    // do_action( 'woocommerce_proceed_to_checkout' );
}

/**
 * Manage WooCommerce styles and scripts.
 */
function grd_woocommerce_script_cleaner() {
    
    wp_dequeue_script( 'selectWoo' );
    wp_deregister_script( 'selectWoo' );
    // wp_dequeue_script( 'wc-country-select' );
    // wp_deregister_script( 'wc-country-select' );
    // echo '<pre>';
    // print_r(wp_print_scripts());
    // echo '</pre>';

}
// add_action( 'wp_enqueue_scripts', 'grd_woocommerce_script_cleaner', 99 );


if( function_exists('acf_add_options_page') ) {
    // add parent
    $parent = acf_add_options_page(array(
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-settings',
        'redirect'      => true
    ));
    // add sub page
    acf_add_options_sub_page(array(
            'page_title'    => 'Header Settings',
            'menu_title'    => 'Header',
            'parent_slug'   => $parent['menu_slug']
    ));
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Footer Settings',
            'menu_title'    => 'Footer',
            'parent_slug'   => $parent['menu_slug'],
    ));
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Product Page',
            'menu_title'    => 'Product Page',
            'parent_slug'   => $parent['menu_slug'],
    ));
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Social',
            'menu_title'    => 'Social',
            'parent_slug'   => $parent['menu_slug'],
    ));
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Subscribe',
            'menu_title'    => 'Subscribe',
            'parent_slug'   => $parent['menu_slug'],
    ));

}

function remove_menus(){
  remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'edit-comments.php' );          //Comments
  remove_menu_page( 'themes.php' );                 //Appearance
}
// add_action( 'admin_menu', 'remove_menus' );

function array_exists($arr){
    if(is_array($arr) && count($arr)>0)
        return true;
    else
        return false;
}

function my_login_logo() {
    echo '<style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url('.get_stylesheet_directory_uri().'/assets/images/logo_2pluso.png);
            background-size: 100%;
            width:110px;
            height:90px;
        }
    </style>';
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function remove_wp_logo( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'wp-logo' );
}
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

// Admin footer modification
add_filter('admin_footer_text', 'remove_footer_admin');
function remove_footer_admin ()
{
    echo '<span id="footer-thankyou">Developed by <a href="http://www.ripplewerkz.com" target="_blank">Ripplewerkz</a></span>';
}
add_filter( 'wp_image_editors', 'change_graphic_lib' );

function special_nav_class ($classes, $item) {

    if(get_post_type()){
    $is_child = preg_match("/".get_post_type()."/i", $item->title);
    }else{
        $is_child = false;
    }

  if (in_array('current-menu-item', $classes) || in_array('current_page_item', $classes) || $is_child){
      $classes[] = 'active ';
  }
  return $classes;
}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

// define the woocommerce_dropdown_variation_attribute_options_html callback 
function filter_woocommerce_dropdown_variation_attribute_options_html( $html, $args ) {

    if( $args['attribute'] == 'pa_size' ) {

        $html = str_replace('<select', '<div class="pr-form-group fz-14">
                                <div class="row font-b">
                                    <div class="col-6 form-group">
                                        <div class="bp-select pr-select select-size"><select', $html);
        $html = str_replace('</select>', '</select></div>
                                    </div>
                                    <div class="col-6 form-group">
                                        <a class="button btn-gray4 btn-border btn-size-3 btn-wide" href="/size-guide" target="_blank">'.esc_html__('Size Guide').'</a>
                                    </div>
                                </div>
                            </div>', $html);
    }

    return $html; 
}; 
add_filter( 'woocommerce_dropdown_variation_attribute_options_html', 'filter_woocommerce_dropdown_variation_attribute_options_html', 10, 2 );

/**
 * @param array      $array
 * @param int|string $position
 * @param mixed      $insert
 */
function array_insert(&$array, $position, $insert)
{
    if (is_int($position)) {
        array_splice($array, $position, 0, $insert);
    } else {
        $pos   = array_search($position, array_keys($array));
        $array = array_merge(
            array_slice($array, 0, $pos),
            $insert,
            array_slice($array, $pos)
        );
    }
}

function custom_woocommerce_checkout_fields($fields) {

    $a_date[] = 'Date';
    for ($i=1; $i <= 31; $i++) { 
        $a_date[$i] = $i;
    }
    $fields['billing']['billing_dob_date'] = array(
        'label'         => __('Date of Birth', 'woocommerce'),
        'required'      => false,
        'clear'         => false,
        'type'          => 'select',
        'priority'      => 21,
        'options'       => $a_date
    );
    $a_month_list = [
                        'January',
                        'February',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December'
                    ];
    $a_month[] = 'Month';
    for ($i=1; $i <= 12; $i++) { 
        $a_month[$i] = $a_month_list[$i-1];
    }
    $fields['billing']['billing_dob_month'] = array(
        'label'         => __(' ', 'woocommerce'),
        'required'      => false,
        'clear'         => false,
        'type'          => 'select',
        'priority'      => 22,
        'options'       => $a_month
    );
    $a_year[] = 'Year';
    for ($i=date('Y')-7; $i > date('Y')-50; $i--) { 
        $a_year[$i] = $i;
    }
    $fields['billing']['billing_dob_year'] = array(
        'label'         => __(' ', 'woocommerce'),
        'required'      => false,
        'clear'         => false,
        'type'          => 'select',
        'priority'      => 23,
        'options'       => $a_year
    );

    $fields['account']['account_password']['label'] = esc_html__( 'Password', 'woocommerce' );
    $fields['account']['account_password']['placeholder'] = esc_html__( 'Your password', 'woocommerce' );
    $fields['account']['account_password']['required'] = false;

    return $fields;
}
add_filter('woocommerce_checkout_fields', 'custom_woocommerce_checkout_fields');


function custom_woocommerce_billing_fields($fields) {

    $a_date[] = 'Date';
    for ($i=1; $i <= 31; $i++) { 
        $a_date[$i] = $i;
    }
    $fields['billing_dob_date'] = array(
        'label'         => __('Date of Birth', 'woocommerce'),
        'required'      => false,
        'clear'         => false,
        'type'          => 'select',
        'priority'      => 21,
        'options'       => $a_date
    );
    $a_month_list = [
                        'January',
                        'February',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December'
                    ];
    $a_month[] = 'Month';
    for ($i=1; $i <= 12; $i++) { 
        $a_month[$i] = $a_month_list[$i-1];
    }
    $fields['billing_dob_month'] = array(
        'label'         => __(' ', 'woocommerce'),
        'required'      => false,
        'clear'         => false,
        'type'          => 'select',
        'priority'      => 22,
        'options'       => $a_month
    );
    $a_year[] = 'Year';
    for ($i=date('Y')-7; $i > date('Y')-50; $i--) { 
        $a_year[$i] = $i;
    }
    $fields['billing_dob_year'] = array(
        'label'         => __(' ', 'woocommerce'),
        'required'      => false,
        'clear'         => false,
        'type'          => 'select',
        'priority'      => 23,
        'options'       => $a_year
    );

    return $fields;
}

add_filter('woocommerce_billing_fields', 'custom_woocommerce_billing_fields');

add_filter( 'woocommerce_update_order_review_fragments', 'my_custom_shipping_table_update');
function my_custom_shipping_table_update( $fragments ) {

    ob_start();
    wc_get_template( 'checkout/shipping-method.php', array(
        'checkout' => WC()->checkout(),
    ) );
    $woocommerce_shipping_methods = ob_get_clean();
    $fragments['.woocommerce-checkout-review-order-table'] = $woocommerce_shipping_methods;

    ob_start();
    wc_get_template( 'checkout/review-order-total.php', array(
        'checkout' => WC()->checkout(),
    ) );
    $woocommerce_shipping_methods = ob_get_clean();
    $fragments['.woocommerce-checkout-review-order-table-total'] = $woocommerce_shipping_methods;

    return $fragments;
}

// define the woocommerce_customer_default_location_array callback 
function filter_woocommerce_customer_default_location_array( $location ) {
    session_start();
    if($_SESSION['customer']['country'])
        $location['country'] = $_SESSION['customer']['country'];
    return $location; 
}; 
         
// add the filter 
add_filter( 'woocommerce_customer_default_location_array', 'filter_woocommerce_customer_default_location_array', 10, 1 );

function my_ajax_update_country_handler() {
    $current_url = $_POST['current_url'];
    $country = $_POST['country'];

    global $woocommerce;
    global $WOOCS;

    $geo_rules = $WOOCS->get_geo_rules();

    foreach ($geo_rules as $rule_key => $rule) {
        if(in_array_r($country,$rule)){
            $WOOCS->set_currency($rule_key);
            break;
        }
    }

    $woocommerce->customer->set_country($country);

    $_SESSION['customer']['country'] = $country;

    $query = parse_url($current_url, PHP_URL_QUERY);

    $url_parts = parse_url($current_url);
    parse_str($url_parts['query'], $params);

    if($country == 'HK' || $country == 'TW'){
        $params['lang'] = 'zh-hant';
    }else if($country == 'CN'){
        $params['lang'] = 'zh-hans';
    }else if($country == 'KR'){
        $params['lang'] = 'ko';
    }else{
        unset($params['lang']);
    }

    $url_parts['query'] = http_build_query($params);
    echo http_build_url($url_parts);
    wp_die();
}

add_action( 'wp_ajax_update_country', 'my_ajax_update_country_handler' );
add_action( 'wp_ajax_nopriv_update_country', 'my_ajax_update_country_handler' );

function get_all_country_dropdown() {

    global $woocommerce;

    $countries = $woocommerce->countries->get_shipping_countries();

    $shipping_country = $woocommerce->customer->get_country();

    $html = '<div class="bp-select-- fz-14 select-country"><select>';
    if($countries){
        foreach ($countries as $code => $country){
            $selected = $shipping_country==$code ? 'selected disabled':'';
            $html .= '<option value="'.$code.'" '.$selected.' data-icon="'.get_template_directory_uri() . '/assets/images/flags/'.strtolower($code).'.png">'.$country.'</option>';
        }
    }
    $html .= '</select></div>';
    echo $html;
}

function get_current_country_lang() {

    global $woocommerce;

    $shipping_country = $woocommerce->customer->get_country();

    $html = '<div class="icon">';
    $html .= '<img src="'.get_template_directory_uri() . '/assets/images/flags/'.strtolower($shipping_country).'.png" width="25">';
    $html .= '</div>';
    $html .= '<div>'.ICL_LANGUAGE_NAME.'</div>';

    echo $html;
}

function get_all_languages_dropdown() {
    if( function_exists('icl_get_languages') ){
        $languages = icl_get_languages('skip_missing=0');
        $html = '<div class="bp-select-- cf-select-- select2 fz-14 select-language"><select>';
        if(1 < count($languages)){
            foreach ($languages as $key => $lang){
                $selected = $lang['active'] ? 'selected disabled':'';
                $html .= '<option value="'.$lang['url'].'" '.$selected.' data-href="'.$lang['url'].'">'.$lang['native_name'].'</option>';
            }
        }
        $html .= '</select></div>';
        echo $html;
    }
}

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/* Function that returns custom product hyperlink */
function wc_cart_item_name_hyperlink( $link_text, $product_data ) {
   $title = get_the_title($product_data['product_id']);
   return $title;
}
/* Filter to override cart_item_name */
add_filter( 'woocommerce_cart_item_name', 'wc_cart_item_name_hyperlink', 10, 2 );

function bootstrap_styles_for_gravityforms_fields($content, $field, $value, $lead_id, $form_id){
    if($field["type"] == 'text') {
        $content = str_replace('<input ', '<input class=\'form-control cf-input \' ', $content);
    }
    if($field["type"] == 'email') {
        $content = str_replace('<input ', '<input class=\'form-control cf-input \' ', $content);
    }
    if($field["type"] == 'textarea') {
        $content = str_replace('class=\'textarea', 'class=\'form-control cf-input ', $content);
    }
    if($field["type"] == 'select') {
        $content = str_replace('gfield_select\'', 'form-control cf-input\' ', $content);
    }
    if($field["type"] == 'mailchimp') {
        $content = str_replace('gchoice', 'cf-checkbox gchoice', $content);
    }
    if($field["type"] == 'select') {
        $content = str_replace('<select', '<div class="bp-select cf-select"><select', $content);
        $content = str_replace('</select>', '</select></div>', $content);
    }
    return $content;
}
add_filter("gform_field_content", "bootstrap_styles_for_gravityforms_fields", 10, 5);

function form_submit_button( $button, $form ) {

    if($form['id'] == 3){
        $button  = '<div class="form-group"><button type="submit" id="gform_submit_button_'.$form['id'].'" class="button btn-blue btn-size-3 fw-600 bp-tt fz-16">'.$form['button']['text'].'</button></div>';
    }else{
        $button  = '<div class="form-group text-center"><button type="submit" id="gform_submit_button_'.$form['id'].'" class="button btn-black btn-size-1 bp-tt fw-600 bp-set">'.$form['button']['text'].'</button></div>';
    }


  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );

class description_walker extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth = 0, $args = array(), $id=0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="'. esc_attr( $class_names ) . '"';


        if(in_array('menu-item-has-children',$classes)){
            $output .= $indent . '<div toggle-set id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes  .= ' class="hf-li-col title"';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
                
            $prepend = '';
            $append = '';
            $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

            if($depth != 0)
            {
                     $description = $append = $prepend = "";
            }

            $item_output = $args->before;
            $item_output .= '<div toggle-btn'. $attributes .'>';
            $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
            $item_output .= $description.$args->link_after;
            $item_output .= '</div>';
            $item_output .= $args->after;

        }else{
            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

            $prepend = '<li>';
            $append = '</li>';
            $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

            if($depth == 0)
            {
                $description = $append = $prepend = "";
                $attributes  .= ' class="hf-li-col title"';
            }

            $item_output = $args->before;
            $item_output .= $prepend.'<a'. $attributes  . $class_names .'>';
            $item_output .= $args->link_before .apply_filters( 'the_title', $item->title, $item->ID );
            $item_output .= $description.$args->link_after;
            $item_output .= '</a>'.$append;
            $item_output .= $args->after;
        }


        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        if(in_array('menu-item-has-children',$classes)){
            $output .= "</div>{$n}";
        }else{
            $output .= "{$n}";
        }
    }

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );
     
        $classes = array( 'fl-li-list fz-12' );
     
        $class_names = join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
     
        $output .= "{$n}{$indent}<div toggle-main><ul$class_names>{$n}";
    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );
        $output .= "$indent</ul></div>{$n}";
    }
}


add_filter( 'wc_stripe_elements_styling', 'marce_add_stripe_elements_styles' );
function marce_add_stripe_elements_styles($array) {
    $array = array(
        'base' => array( 
            'fontFamily'    => 'Montserrat,Helvetica,Calibri',
            'fontSize'      => '14px',
            'fontWeight'    => '400',
            'lineHeight'    => '21px',
            '::placeholder' => [
                'fontStyle' => 'italic',
            ]

        ),
        'invalid' => array(
            'color'     => '#b81c23'
        )
    );
    return $array;
}

function lit_woocommerce_confirm_password_validation( $posted ) {
    $checkout = WC()->checkout;
    if ( ! is_user_logged_in() && ( $checkout->must_create_account || ! empty( $posted['createaccount'] ) ) ) {
        if ( strcmp( $posted['account_password'], $posted['createaccount'] ) !== 0 ) {
            wc_add_notice( __( '<strong>Password</strong> do not match.', 'woocommerce' ), 'error' ); 
        }
    }
}
add_action( 'woocommerce_after_checkout_validation', 'lit_woocommerce_confirm_password_validation', 10, 2 );

function lit_woocommerce_confirm_password_checkout( $checkout ) {
    if ( get_option( 'woocommerce_registration_generate_password' ) == 'no' ) {

        $fields = $checkout->get_checkout_fields();

        $fields['account']['createaccount'] = array(
            'type'              => 'password',
            'label'             => __( 'Repeat password', 'woocommerce' ),
            'placeholder'       => _x( 're-type your password', 'placeholder', 'woocommerce' )
        );

        $checkout->__set( 'checkout_fields', $fields );
    }
}
add_action( 'woocommerce_checkout_init', 'lit_woocommerce_confirm_password_checkout', 10, 1 );

/**
 * Hide shipping rates when free shipping is available.
 * Updated to support WooCommerce 2.6 Shipping Zones.
 *
 * @param array $rates Array of rates found for the package.
 * @return array
 */
function my_hide_shipping_when_free_is_available( $rates ) {
    $free = array();
    foreach ( $rates as $rate_id => $rate ) {
        if ( 'free_shipping' === $rate->method_id ) {
            $free[ $rate_id ] = $rate;
            break;
        }
    }
    return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );

function action_mc4wp_integration_before_checkbox_wrapper( $instance ) { 
    if($instance->options['position'] == 'checkout_billing'){

        echo  '<div class="container-fluid mb10 animate">
                <div class="container width-1">
                    <div class="row">
                        <div class="col-sm-8 cf-checkbox fz-16 mt1">';
    }else{
        echo '<div class="cf-checkbox fz-14">';
    }

};       
function action_mc4wp_integration_after_checkbox_wrapper( $instance ) { 
    if($instance->options['position'] == 'checkout_billing'){
        echo  '</div></div></div></div>';
    }else{
        echo '</div>';
    }

};
add_action( 'mc4wp_integration_before_checkbox_wrapper', 'action_mc4wp_integration_before_checkbox_wrapper', 10, 1 );
add_action( 'mc4wp_integration_after_checkbox_wrapper', 'action_mc4wp_integration_after_checkbox_wrapper', 10, 1 );

function cinchws_filter_dropdown_args( $args ) {
    $args['show_option_none'] = __('Size', 'woocommerce');
    return $args;
}
add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'cinchws_filter_dropdown_args', 10 );


function acf_load_country_field_choices( $field ) {

    global $woocommerce;

    $field['choices'] = $woocommerce->countries->get_shipping_countries();
    if( is_array($choices) ) {
        foreach( $choices as $choice ) {
            $field['choices'][ $choice ] = $choice;
        }
    }
    // return the field
    return $field;
}
add_filter('acf/load_field/name=country', 'acf_load_country_field_choices');

// Change sender name
// add_filter( 'woocommerce_email_from_name', function( $from_name, $wc_email ){
//  print_r($wc_email);
//     if( $wc_email->id == 'customer_processing_order' )
//         $from_name = 'Jack the Ripper';

//     return $from_name;
// }, 10, 2 );

// Change sender adress
add_filter( 'woocommerce_email_from_address', function( $from_email, $wc_email ){
    if( $wc_email->id == 'customer_refunded_order' ){
        $from_email = 'return@twopluso.com';
    }elseif( $wc_email->id == 'customer_reset_password' ){
        $from_email = 'support@twopluso.com';
    }
    return $from_email;
}, 10, 2 );

function my_woocs_currency_data_manipulation($currencies)
{
    if($_SERVER['REQUEST_URI'] != '/wp-admin/admin.php?page=wc-settings&tab=woocs'){
        foreach ($currencies as $key => $value)
        {
            if ($key != 'SGD')
            {
                $currencies[$key]['rate'] = $value['rate'] + 0.05*$value['rate'];//add 5%
                // break;
            }
        }
    }
    return $currencies;
}
add_filter('woocs_currency_data_manipulation', 'my_woocs_currency_data_manipulation', 1, 1);

// define the mc4wp_integration_show_checkbox callback 
function filter_mc4wp_integration_show_checkbox( $show_checkbox, $integration_slug ) { 

    if(!is_user_logged_in()){
        return $show_checkbox;
    }
    return;
}; 
// add the filter 
add_filter( 'mc4wp_integration_show_checkbox', 'filter_mc4wp_integration_show_checkbox', 10, 2 ); 


// add_action( 'init', 'blockusers_init' );
// function blockusers_init() {
//     if ( is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
//         wp_redirect( home_url() );
//         exit;
//     }
// }

function hide_admin_bar_if_non_admin( $show ) {
    if ( ! current_user_can( 'administrator' ) ) $show = false;
    return $show;
}
 
add_filter( 'show_admin_bar', 'hide_admin_bar_if_non_admin', 20, 1 );


// define the woocommerce_email_classes callback 
function filter_woocommerce_email_classes( $array ) { 

    unset($array['WC_Email_Customer_Refunded_Order']);

    return $array; 
}; 
         
// add the filter 
// add_filter( 'woocommerce_email_classes', 'filter_woocommerce_email_classes', 10, 1 ); 

/**
 * To display additional field at My Account page 
 * Once member login: edit account
 */
add_action( 'woocommerce_edit_birthdate_form', 'my_woocommerce_edit_account_form' );

function my_woocommerce_edit_account_form() {

    $user_id = get_current_user_id();
    $user = get_userdata( $user_id );

    if ( !$user )
        return;

    $account_dob_date = get_user_meta( $user_id, 'account_dob_date', true );
    $account_dob_month = get_user_meta( $user_id, 'account_dob_month', true );
    $account_dob_year = get_user_meta( $user_id, 'account_dob_year', true );


    $a_date[] = 'Date';
    for ($i=1; $i <= 31; $i++) { 
        $a_date[$i] = $i;
    }
    $a_month_list = [
                        'January',
                        'February',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December'
                    ];
    $a_month[] = 'Month';
    for ($i=1; $i <= 12; $i++) { 
        $a_month[$i] = $a_month_list[$i-1];
    }
    $a_year[] = 'Year';
    for ($i=date('Y')-7; $i > date('Y')-50; $i--) { 
        $a_year[$i] = $i;
    }

?>

<div class="form-group no-margin">
    <label for=""><?php esc_html_e( 'Date of Birth', 'woocommerce' ); ?></label>
    <div class="row">
        <div class="col-sm-4 form-group">
            <div class="bp-select cf-select">
                <select name="account_dob_date" id="account_dob_date">
                    <?php foreach($a_date as $date_key => $date): ?>
                        <option <?php echo $date_key == esc_attr( $account_dob_date )? 'selected':''; ?>  value="<?php echo $date_key; ?>"><?php echo $date; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-sm-4 form-group">
            <div class="bp-select cf-select">
                <select name="account_dob_month" id="account_dob_month">
                    <?php foreach($a_month as $month_key => $month): ?>
                        <option <?php echo $month_key == esc_attr( $account_dob_month )? 'selected':''; ?>  value="<?php echo $month_key; ?>"><?php echo $month; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-sm-4 form-group"><?php echo esc_attr( $user->dob_year ); ?>
            <div class="bp-select cf-select">
                <select name="account_dob_year" id="account_dob_year">
                    <?php foreach($a_year as $year_key => $year): ?>
                        <option <?php echo $year_key == esc_attr( $account_dob_year )? 'selected':''; ?> value="<?php echo $year_key; ?>"><?php echo $year; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>
</div>


<?php
 
} // end func

/**
 * This is to save user input into database
 * hook: woocommerce_save_account_details
 */
add_action( 'woocommerce_save_account_details', 'my_woocommerce_save_account_details' );

function my_woocommerce_save_account_details( $user_id ) {
    update_user_meta( $user_id, 'account_dob_date', htmlentities( $_POST[ 'account_dob_date' ] ) ); 
    update_user_meta( $user_id, 'account_dob_month', htmlentities( $_POST[ 'account_dob_month' ] ) ); 
    update_user_meta( $user_id, 'account_dob_year', htmlentities( $_POST[ 'account_dob_year' ] ) ); 
} // end func

function wc_remove_link_on_thumbnails( $html ) {
    return preg_replace('#<a.*?>(.*?)</a>#i', '\1', $html);
}
add_filter('woocommerce_single_product_image_thumbnail_html','wc_remove_link_on_thumbnails' );


add_action( 'init', 'process_post' );

function process_post() {
    global $WOOCS;
    if( $WOOCS ) {
        $_SESSION['currency'] = $WOOCS->current_currency;
    }
}

function change_graphic_lib($args) {
    return $args;
}