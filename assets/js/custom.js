'use strict';
// @koala-prepend "TweenMax.min.js"
// @koala-prepend "jquery.flexslider-min.js"
// @koala-prepend "owl.carousel.min.js"
// @koala-prepend "jquery.sticky-kit.min.js"
// @koala-prepend "jquery.stellar.min.js"
// @koala-prepend "jquery.fancybox.min.js"
var $ = jQuery;
var bp = {
	width: 0,
	height: 0,
	headerHeight: 0,
	footerHeight: 0,
	hash: '',
	browser: '',
	ismobile: false,
	init: function() {
		bp.gethash();
		bp.browserdetect();
		bp.mobiledetect();

		// for mobile
		if(bp.ismobile) {
			$('html').addClass('bp-touch');
		}

		bp.loader('init');
		bp.resize();
		bp.inview();
		bp.parallax();
		bp.contentsliding();
		bp.popup();
	},
	ready: function() {
		bp.resize();

		// hide the preloader
		bp.loader('close');
		bp.header();
		bp.animation();

		$('.bp-select').each(function(index) {
			var _class = 'bpselect' + index;
			$(this).addClass(_class);

			bp.select({
				target: '.' + _class
			});
		});

		$('[toggle-group]').each(function(index) {
			var _this = $(this),
			_class = 'togglegroup' + index,
			_type = $(this).attr('toggle-group');
			_this.addClass(_class);

			if(_type == 'type1') {
				bp.togglegroup({
					target: '.' + _class,
					firstactive: false,
					onetime: false,
					animation: 'none'
				});
			}

			if(_type == 'type2') {
				bp.togglegroup({
					target: '.' + _class,
					firstactive: true,
					onetime: true,
					animation: 'slide'
				});
			}

			if(_type == 'type3') {
				bp.togglegroup({
					target: '.' + _class,
					firstactive: false,
					onetime: true,
					animation: 'slide'
				});
			}

			if(_type == 'type4') {
				bp.togglegroup({
					target: '.' + _class,
					firstactive: false,
					onetime: true,
					animation: 'none',
					autohide: true
				});
			}
		});

		$('[product-carousel]').each(function() {
			var _this = $(this),
			_type = _this.attr('product-carousel');

			if(_type == 'type1') {
				$('.owl-carousel', _this).owlCarousel({
					loop: true,
					nav: true,
					smartSpeed: 700,
					responsive: {
						0: {
							items: 1,
							margin: 20,
							dots: true,
						},
						640: {
							items: 2,
							margin: 20,
							dots: false,
						},
						768: {
							items: 3,
							margin: 50,
							dots: false,
						}
					}
				});
			}

			if(_type == 'type2') {
				$('.owl-carousel', _this).owlCarousel({
					items: 1,
					margin: 0,
					loop: true,
					nav: true,
					dots: true,
					autoplay: false,
					smartSpeed: 700,
					autoplaySpeed: 1000
				});
			}
		});

		bp.home();

		bp.storelocator({
			target: '.store-locator-content',
		});

		$('[sticky-content]').stick_in_parent({
			offset_top: (bp.headerHeight + 20)
		});

		bp.products();

	},
	loader: function(state) {
		var _loader = {
			content: '',
			init: function() {
				_loader.content = '<div class="bp-preloader"><div class="spinner"><span class="s-dot s-dot1"></span><span class="s-dot s-dot2"></span></div></div>';
				$('body').prepend(_loader.content);
			},
			close: function() {
				$('.bp-preloader').fadeOut();
			}
		}

		if(state == 'init') {
			_loader.init();
		} else if(state == 'close') {
			_loader.close();
		}
	},
	resize: function() {
		var _resize = {
			init: function() {
				bp.width = $(window).outerWidth();
				bp.height = $(window).outerHeight();
				bp.headerHeight = $('.header-content').outerHeight();
				bp.footerHeight = $('footer').outerHeight();

				// STICKY FOOTER
				var footerTop = (bp.footerHeight) * -1;
				$('footer').css({marginTop: footerTop});
				$('#main-wrapper').css({paddingBottom: bp.footerHeight});
				$('.push-top').css({paddingTop: bp.headerHeight});

				$('.height-main').css({height: bp.height - bp.headerHeight});
				$('.height-full').css({height: bp.height});

				// for equal height
				$('[group-height]').each(function() {
					var _this = $(this);

					bp.equalize($('.gh1', _this));
				});

				// for floating
				$('.he-co-floating.main').css({top: 0, height: bp.height});
				if(bp.width > 950) {
					$('.he-co-floating.main').css({top: bp.headerHeight, height: bp.height - bp.headerHeight});
				}
			},
			
		}
		_resize.init();

		if( bp.width <= 767 ) {
	    	$('.bp-desktop2').remove();
		}else{
	    	$('.bp-mobile2').remove();
		}

	},
	equalize: function(target) {
		$(target).css({minHeight: 0});
		var _biggest = 0;
		for ( var i = 0; i < target.length ; i++ ){
			var element_height = $(target[i]).outerHeight();
			if(element_height > _biggest ) _biggest = element_height;
		}
		$(target).css({minHeight: _biggest});
		return _biggest;
	},
	gethash: function() {
		bp.hash = window.location.hash;
	},
	browserdetect: function() {
		var BrowserDetect = {
		    init: function () {
		        this.browser = this.searchString(this.dataBrowser) || "Other";
		        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";

		        $('html').addClass(BrowserDetect.browser);
				bp.browser = BrowserDetect.browser;
		    },
		    searchString: function (data) {
		        for (var i = 0; i < data.length; i++) {
		            var dataString = data[i].string;
		            this.versionSearchString = data[i].subString;

		            if (dataString.indexOf(data[i].subString) !== -1) {
		                return data[i].identity;
		            }
		        }
		    },
		    searchVersion: function (dataString) {
		        var index = dataString.indexOf(this.versionSearchString);
		        if (index === -1) {
		            return;
		        }

		        var rv = dataString.indexOf("rv:");
		        if (this.versionSearchString === "Trident" && rv !== -1) {
		            return parseFloat(dataString.substring(rv + 3));
		        } else {
		            return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
		        }
		    },

		    dataBrowser: [
		        {string: navigator.userAgent, subString: "Edge", identity: "ms-edge"},
		        {string: navigator.userAgent, subString: "MSIE", identity: "explorer"},
		        {string: navigator.userAgent, subString: "Trident", identity: "explorer"},
		        {string: navigator.userAgent, subString: "Firefox", identity: "firefox"},
		        {string: navigator.userAgent, subString: "Opera", identity: "opera"},  
		        {string: navigator.userAgent, subString: "OPR", identity: "opera"},  

		        {string: navigator.userAgent, subString: "Chrome", identity: "chrome"}, 
		        {string: navigator.userAgent, subString: "Safari", identity: "safari"}       
		    ]
		};
		BrowserDetect.init();
	},
	mobiledetect: function() {
		var isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};
		if(isMobile.any) {
			bp.ismobile = isMobile.any();
		}
	},
	select: function(opt) {
		var _select = {
			target: '',
			box: '',
			title: '',
			list: '',
			init: function() {
				_select.defaults();
				_select.create();
				_select.clicks();
			},
			defaults: function() {
				_select.target = document.querySelector(opt.target);
				_select.box = document.querySelector(opt.target + ' > select');

				if(bp.ismobile && bp.width < 1020) {
					$(_select.target).addClass('select-mobile');
				}
			},
			create: function() {
				// create title
				_select.title = document.createElement('div');
				$(_select.title).addClass('bp-se-col title');

				// create list
				_select.list = document.createElement('ul');
				$(_select.list).addClass('bp-se-col list');

				$('option', _select.box).each(function(index) {
					var _this = $(this),
					_value = _this.attr('value'),
					_text = _this.html(),
					_icon = _this.attr('data-icon'),
					_country = _this.attr('data-country'),
					_href = _this.attr('data-href');

					var _entry = document.createElement('li');
					$(_entry).attr('select-number', index);

					if(_this.attr('selected') == true || _this.attr('selected') == 'selected') {
						$(_entry).addClass('selected');
					}

					if(_this.attr('disabled') == true || _this.attr('disabled') == 'disabled') {
						$(_entry).addClass('disabled');
					}

					if(_icon) {
						var _iconTemp = document.createElement('div');
						$(_iconTemp).addClass('icon');

						var _iconImg = document.createElement('img');
						$(_iconImg).attr('src', _icon);
						$(_iconTemp).append(_iconImg);

						$(_entry).append(_iconTemp);
					}

					if(_country) {
						$(_entry).attr('data-country',_country);
					}
					if(_href) {
						$(_entry).attr('data-href',_href);
					}

					var _textTemp = document.createElement('div');
					$(_textTemp).append(_text);
					$(_entry).append(_textTemp);

					$(_select.list).append(_entry);
				});

				// append the containers
				$(_select.target).append(_select.title);
				$(_select.target).append(_select.list);
			},
			clicks: function() {
				var _index = 0;

				$('li', _select.list).click(function() {
					_select.open(this);
					_select.close();
				});

				$('option', _select.box).each(function(index) {
					var _this = $(this);

					if(_this.attr('selected') == true || _this.attr('selected') == 'selected') {
						_index = index;
					}
				});

				$('li', _select.list).eq(_index).trigger('click');

				$(_select.title).click(function() {
					$(_select.target).toggleClass('select-active');
				});

				// for native select box
				$(_select.box).on('change', function() {
					var _num = this.selectedIndex,
					_li = $('li', _select.list).eq(_num);

					_select.open(_li, _num);
					_select.close();
				});

				// when click outside the checkbox
				window.addEventListener('click', function(e){   
					if (!(document.querySelector(opt.target).contains(e.target))){
						_select.close();
					}
				});
			},
			open: function(target, number) {
				$(_select.target).addClass('select-active');

				if(!(number)) {
					number = $(target).attr('select-number');
				}
				var _curOpt = $('select option', _select.target).eq(number);

				// old way of setting the active selected option 2018011901
				// $('option', _select.box).attr('selected', false);
				// _curOpt.attr('selected', true);

				// new way of setting selected option 2018012001
				_select.box.options.selected = false;
				_select.box.options[number].selected = true;

				$(_select.title).html('');

				var _icon = _curOpt.attr('data-icon');
				if(_icon) {
					var _iconTemp = document.createElement('div');
					$(_iconTemp).addClass('icon');

					var _iconImg = document.createElement('img');
					$(_iconImg).attr('src', _icon);
					$(_iconTemp).append(_iconImg);

					$(_select.title).append(_iconTemp);
				}

				var _textTemp = document.createElement('div'),
				_text = _curOpt.html();

				$(_textTemp).append(_text);
				$(_select.title).append(_textTemp);

				$('li', _select.list).removeClass('selected');
				$('li', _select.list).eq(number).addClass('selected');
			},
			close: function() {
				$(_select.target).removeClass('select-active');
				// $(_select.list).hide();
			}
		}
		_select.init();
	},
	togglegroup: function(opt) {
		var _toggle = {
			target: '',
			firstactive: true,
			onetime: true,
			animation: 'toggle', // none, toggle, slide
			hash: [],
			autohide: false,
			init: function() {
				_toggle.defaults();
				_toggle.clicks();
				_toggle.autoopen();
			},
			defaults: function() {
				_toggle.target = document.querySelector(opt.target);

				// if first entry shud be open upon page load
				if(opt.firstactive == false) {
					_toggle.firstactive = false;
				}

				// if one dropdown shud be open at a time
				if(opt.onetime == false) {
					_toggle.onetime = false;
				}

				// animation can be 'none', 'toggle' or 'slide'
				if(opt.animation) {
					_toggle.animation = opt.animation;
				}

				// if toggle main will hide if click outside
				if(opt.autohide == true) {
					_toggle.autohide = true;
				}

				// collect the ids of the buttons
				$('[toggle-btn]', _toggle.target).each(function() {
					var _hash = $(this).attr('toggle-btn');
					_toggle.hash.push(_hash);
				});
			},
			clicks: function() {
				$('[toggle-btn]', _toggle.target).click(function(e) {
					e.preventDefault();
					var _parent = $(this).closest('[toggle-set]');

					if(_parent.hasClass('toggle-active')) {
						_toggle.close(_parent);
					} else {
						_toggle.open(_parent);
					}
				});

				// when click outside the toggle set
				if(_toggle.autohide) {
					window.addEventListener('click', function(e){
						var _active = $('[toggle-set].toggle-active', _toggle.target).eq(0);
						if(!$(e.target).closest(_active).length) {
							_toggle.close(_active);
						}
					});
				}
			},
			autoopen: function() {
				var _hash = bp.hash.split('#');

				if($.inArray(_hash[1], _toggle.hash) >= 0) {
					// open the target item by url hash
					var _id = $('[toggle-btn="'+ _hash[1] +'"]', _toggle.target);
					_id.trigger('click');
					$('body, html').animate({scrollTop: _id.offset().top -(bp.headerHeight + 20)}, 1000); 
				} else {
					var _cur = $('[toggle-set].toggle-active', _toggle.target).eq(0);

					if(_cur.length == 1) {
						// $('[toggle-btn]', _cur).trigger('click');

						_toggle.open(_cur);
					} else {
						// open the first item
						if(_toggle.firstactive) {
							$('[toggle-btn]', _toggle.target).eq(0).trigger('click');
						}
					}
				}
			},
			open: function(target) {
				var _parent = $(target).closest('[toggle-group]'),
				_curActive = $('[toggle-set].toggle-active', _parent).eq(0);

				if(!(_toggle.animation == 'none')) {
					if(_toggle.onetime) {
						_curActive.removeClass('toggle-active');
						$('[toggle-main]', _curActive).slideUp(150);
					}
				}

				$(target).addClass('toggle-active');

				if(_toggle.animation == 'toggle') {
					$('[toggle-main]', target).show();
				}

				if(_toggle.animation == 'slide') {
					$('[toggle-main]', target).slideDown(150);
				}
			},
			close: function(target) {
				$(target).removeClass('toggle-active');

				if(_toggle.animation == 'toggle') {
					$('[toggle-main]', target).hide();
				}

				if(_toggle.animation == 'slide') {
					$('[toggle-main]', target).slideUp(150);
				}
			}
		}
		_toggle.init();
	},
	inview: function() {
		(function(d){var p={},e,a,h=document,i=window,f=h.documentElement,j=d.expando;d.event.special.inview={add:function(a){p[a.guid+"-"+this[j]]={data:a,$element:d(this)}},remove:function(a){try{delete p[a.guid+"-"+this[j]]}catch(d){}}};d(i).bind("scroll resize",function(){e=a=null});!f.addEventListener&&f.attachEvent&&f.attachEvent("onfocusin",function(){a=null});setInterval(function(){var k=d(),j,n=0;d.each(p,function(a,b){var c=b.data.selector,d=b.$element;k=k.add(c?d.find(c):d)});if(j=k.length){var b;
		if(!(b=e)){var g={height:i.innerHeight,width:i.innerWidth};if(!g.height&&((b=h.compatMode)||!d.support.boxModel))b="CSS1Compat"===b?f:h.body,g={height:b.clientHeight,width:b.clientWidth};b=g}e=b;for(a=a||{top:i.pageYOffset||f.scrollTop||h.body.scrollTop,left:i.pageXOffset||f.scrollLeft||h.body.scrollLeft};n<j;n++)if(d.contains(f,k[n])){b=d(k[n]);var l=b.height(),m=b.width(),c=b.offset(),g=b.data("inview");if(!a||!e)break;c.top+l>a.top&&c.top<a.top+e.height&&c.left+m>a.left&&c.left<a.left+e.width?
		(m=a.left>c.left?"right":a.left+e.width<c.left+m?"left":"both",l=a.top>c.top?"bottom":a.top+e.height<c.top+l?"top":"both",c=m+"-"+l,(!g||g!==c)&&b.data("inview",c).trigger("inview",[!0,m,l])):g&&b.data("inview",!1).trigger("inview",[!1])}}},250)})(jQuery);
	},
	animation: function() {
		$('.animate').each(function() {
			var thisAnim = $(this), // this content
			animControl = $(this).attr('anim-control'), // for single or parent container
			animName = $(this).attr('anim-name'), // customize animation
			animDelay = parseFloat($(this).attr('anim-delay')), // delay value
			delayIncrease = 0.2;
			// if the container is parent
			if(animControl == 'parent') {
				// add delay to each child element
				$(this).children().each(function(index) {
					var element = $(this);
					if(isNaN(animDelay)) {
						animDelay = 0.5;
					}
					if(animDelay == 0) {
	                    delayIncrease = 0;
	                }
					var delayNum = animDelay + (delayIncrease * index) + 's';
					setTimeout(function() {
						$(element).css('-webkit-animation-delay', delayNum)
							.css('-moz-animation-delay', delayNum)
							.css('-ms-animation-delay', delayNum)
							.css('-o-animation-delay', delayNum)
					}, 100);
				});
				$(this).css({opacity: 1});

				if(animName == null) {
					// if no customize animation then use default animation
					$(this).children().each(function(index) {
						$(this).addClass('anim-content');
					});
				} else {
					// if have customize animation
					$(this).children().each(function(index) {
						$(this).addClass('animated');
						$(this).on('inview',function(event,visible){
							if (visible == true) {
								$(this).addClass(animName);
							}
						});
					});
				}

			// if the container is not parent
			} else {
				if(animName == null) {
					// if no customize animation then use default animation
					$(this).addClass('anim-content');
				} else {
					// if have customize animation
					$(this).addClass('animated');
					$(this).on('inview',function(event,visible){
						if (visible == true) {
							$(this).addClass(animName);
						}
					});
				}
				// if have customize animation-delay
				if(animDelay != null) {
					if(isNaN(animDelay)) {
						animDelay = 0.5;
					}
					var delayNum = animDelay + 's';
					$(this).css('-webkit-animation-delay', delayNum)
							.css('-moz-animation-delay', delayNum)
							.css('-ms-animation-delay', delayNum)
							.css('-o-animation-delay', delayNum);
				}
			}

		});

		$.each($('.anim-content, .inview'),function(){
			$(this).on('inview',function(event,visible){
				var _this = $(this),
				_delay = 1000;

				if (visible == true) {
					if(!_this.hasClass('done')) {
						_this.addClass('visible done');
					}

					var _animDelay = _this.css('animation-delay').replace('s','');
					if(_animDelay) {
						_delay = _animDelay * 2000;
					}

					setTimeout(function() {
						_this.removeClass('animate anim-content inview');
					}, _delay);
				}
			});
		});

		$.each($('[anim-control="parent"]'),function(){
			var _this = $(this);

			_this.on('inview',function(event,visible){
				if (visible == true) {
					if(!$('.anim-content', _this).hasClass('done')) {
						$('.anim-content', _this).addClass('visible done');
					}
				}
			});
		});
	},
	parallax: function() {
		if(bp.browser != 'firefox') {
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 50
			});
		} else {
			$('[data-stellar-background-ratio]').addClass('top');
		}
	},
	header: function() {
		var _header = {
			target: '.header-content',
			scrollTop: 0,
			lastScrollTop: 0,
			morethan: false,
			init: function() {
				_header.onScroll();
				_header.userOpt();
				_header.menu();
			},
			onScroll: function() {
				var _scroll = {
					init: function() {
						window.addEventListener('scroll', function() {
							_header.scrollTop = window.pageYOffset || document.documentElement.scrollTop;
							_scroll.update();
						}, false);
					},
					update: function() {
						// for fixed header
						if(!($('header').hasClass('header-fixed'))) {
							if(_header.scrollTop > (bp.headerHeight / 2)) {
								if(!(_header.morethan)) {
									_header.morethan = true;
									$('header').addClass('header-top');
								}
							} else {
								_header.morethan = false;
								$('header').removeClass('header-top');
							}
						} else {
							$('header').addClass('header-top');
						}

						// show/hide the header on mobile
						if (_header.scrollTop > _header.lastScrollTop && _header.scrollTop > (bp.headerHeight * 2)){
							// scrolling down
							$('header').addClass('header-up');
						} else {
							// scrolling up
							$('header').removeClass('header-up');
						}

						_header.lastScrollTop = _header.scrollTop <= 0 ? 0 : _header.scrollTop;
					}
				}
				_scroll.init();
			},
			userOpt: function() {
				var _opt = $('.hc-ro-user-opt');

				// if(bp.ismobile) {
					$('li.sub', _opt).each(function(index) {
						var _this = $(this),
						_index = 'user-opt-' + index;
						_this.addClass(_index);

						$('.hr-us-icon', _this).click(function() {
							_this.toggleClass('hover-active');
							$('.hr-us-sub', _this).toggle();
						});

						// when click outside the checkbox
						window.addEventListener('click', function(e){   
							if (!(document.querySelector('.' + _index).contains(e.target))){
								_this.removeClass('hover-active');
								$('.hr-us-sub', _this).hide();
							}
						});
					});
				// } else {
				// 	$('li.sub', _opt).mouseenter(function() {
				// 		$(this).addClass('hover-active');
				// 		$('.hr-us-sub', this).show();
				// 	}).mouseleave(function() {
				// 		$(this).removeClass('hover-active');
				// 		$('.hr-us-sub', this).stop(true,true).hide();
				// 	});
				// }
			},
			menu: function() {
				$('.menu li.sub > a', _header.target).removeAttr('href');

				$('.menu > ul > li', _header.target).mouseenter(function() {
					$('ul', this).slideDown(150);
				}).mouseleave(function() {
					$('ul', this).stop(true,true).slideUp(150);
				});
			}
		}
		_header.init();
	},
	home: function() {
		var _home = {
			slider: '',
			curSlide: 0,
			totalSlide: 0,
			init: function() {
				_home.slider = $('.home-slider .flexslider');

				_home.slider.flexslider({
					animation: 'fade',
					directionNav: false,
					controlNav: false,
					slideshow: true,
					start: function() {
						_home.totalSlide = $('ul.slides > li', _home.slider).length
						$('ul.slides > li', _home.slider).each(function(index) {
							$(this).attr('slide-number', index);
						});

						_home.nav();
						_home.numbering();
					},
					after: function() {
						_home.numbering();
					}
				});

				$('.hs-su-arrow').click(function() {
					$('body, html').animate({scrollTop: (bp.height - bp.headerHeight)}, 1000);
				});
			},
			nav: function() {
				$('.ho-sl-nav a').click(function(e){
					e.preventDefault();
					var _this = $(this);
					if(_this.hasClass('nav-prev')) {
						_home.slider.flexslider('prev');
					}
					if(_this.hasClass('nav-next')) {
						_home.slider.flexslider('next');
					}

					_home.numbering();
				});
			},
			numbering: function() {
				var _numParent = $('.ho-sl-num');
				_home.curSlide = parseInt($('.flex-active-slide', _home.slider).eq(0).attr('slide-number'));

				$('.num-first', _numParent).html(('0' + (_home.curSlide + 1)).slice(-2));
				$('.num-last', _numParent).html(('0' + _home.totalSlide).slice(-2));
			}
		}
		_home.init();
	},
	storelocator: function(opt) {
		var _map = {
			target: '',
			popup: '',
			infoBox: '',
			mainList: '',
			entrynum: 0,
			boxClose: false,
			boxOffset: 0,
			arrow: '',
			init: function() {
				_map.defaults();
				_map.clicks();

				_map.resize();
				window.addEventListener('resize', function() {
					_map.resize();
				});
			},
			defaults: function() {
				_map.target = opt.target;
				_map.infoBox = $('.sl-co-box', _map.target);
				_map.mainList = $('.sc-bo-col.main', _map.target);
				_map.arrow = $('.sb-co-col.arrow', _map.target);
			},
			clicks: function() {
				$('.sc-bo-set', _map.target).on('click', function() {
					var _this = $(this),
					_active = $('.sc-bo-set.selected', _map.target),
					_mapNum = _this.attr('map-entry');

					if(!(_mapNum)) {
						_map.entrynum++;
						_this.attr('map-entry', _map.entrynum);
					}

					_map.showpopup(_this);

					if(_active.length > 0){
						_map.hidepopup(_active);
					}
				});

				// when click outside the set
				window.addEventListener('click', function(e) {
					var _activeSet = $('.sc-bo-set.selected', _map.target),
					_activePopup = $('.sc-bo-col.popup');

					if(!($(e.target).closest(_activeSet).length == 1 || $(e.target).closest(_activePopup).length == 1)) {
						if(bp.width > 768) {
							_map.hidepopup(_activeSet);
						}
					}
				});

				_map.arrow.click(function() {
					var _this = $(this);

					if(_this.hasClass('active')) {
						_this.removeClass('active');
						_map.boxClose = false;
						TweenMax.to(_map.infoBox, 0.5, {y: 0});
					} else {
						_this.addClass('active');
						_map.boxClose = true;
						TweenMax.to(_map.infoBox, 0.5, {y: _map.boxOffset});
					}
				});
			},
			resize: function() {
				var _parentHeight = _map.infoBox.closest(_map.target).height() - (parseInt(_map.infoBox.parent().css('padding-top')) + parseInt(_map.infoBox.parent().css('padding-bottom'))),
				_padding = 30;

				if (bp.width < 767) {
					_parentHeight = _map.infoBox.closest(_map.target).height() * 0.75;
					_padding = 15;
				}

				_map.infoBox.css({maxHeight: _parentHeight});

				// for mobile box
				_map.boxOffset = _parentHeight - $(_map.arrow).outerHeight();
				if (_map.boxClose) {
					TweenMax.set(_map.infoBox, {y: _map.boxOffset});
				}

				var _listHeight = 0;
				$('> *', _map.infoBox).each(function() {
					var _this = $(this),
					_height = _this.outerHeight();

					if(!(_this.hasClass('sc-bo-col main'))) {
						_listHeight = _listHeight + _height;
					}
				});

				_map.mainList.css({height: _parentHeight - (_listHeight + _padding)});
			},
			showpopup: function(target) {
				$(target).addClass('selected');

				_map.popup = document.createElement('div');
				$(_map.popup).addClass('sc-bo-col popup').attr('popup-number', target.attr('map-entry'));

				var _popupMain = $('.popup-holder', target).html();

				$(_map.popup).append(_popupMain);
				$(_map.infoBox).append(_map.popup);

				var _popupOffset = $(target).offset().top - $(_map.infoBox).offset().top,
				_offsetTop = _popupOffset - parseInt($('.sc-pop-box > .arrow', _map.popup).css('top')) + ($(target).outerHeight() / 2);
				// var _offsetTop = $(target).offset().top - ($(_map.infoBox).offset().top + 202) + ($(target).outerHeight() / 2);

				$('.sb-co-popup', _map.popup).css({top: _offsetTop});

				TweenMax.fromTo($('.sc-pop-box', _map.popup), 0.5, {opacity: 0, x: 20}, {opacity: 1, x: 0});

				// close button for mobile
				$('.sc-bo-close', _map.popup).on('click', function() {
					var _activeSet = $('.sc-bo-set.selected', _map.target);
					_map.hidepopup(_activeSet);
				});
			},
			hidepopup: function(target) {
				$(target).removeClass('selected');

				var _popup = $('[popup-number="'+ $(target).attr('map-entry') +'"]'),
				_popTarget = $('.sc-pop-box', _popup);

				// if mobile, change the content to animate
				if (bp.width < 768) {
					_popTarget = $('.sb-co-popup', _popup);
				}

				TweenMax.to(_popTarget, 0.5, {opacity: 0, x: 20, onComplete: function() {
					$(_popup).remove();
				}});
			}
		}
		_map.init();
	},
	contentsliding: function() {
		var _slide = {
			btn: '',
			init: function() {
				_slide.defaults();

				_slide.btn.click(function() {
					if($(this).hasClass('active')) {
						_slide.inactive();
					} else {
						_slide.active();
					}
				});

				$('.he-co-floating.cover').click(function() {
					_slide.inactive();
				});

				if(_slide.btn.hasClass('active')) {
					_slide.active();
				}
			},
			defaults: function() {
				_slide.btn = $('.menu-bar');
			},
			active: function() {
				_slide.btn.addClass('active');
				$('html').addClass('menu-slide-active');
				if(bp.width < 950) {
					$('.he-co-floating.cover').fadeIn();
				}
			},
			inactive: function() {
				_slide.btn.removeClass('active');
				$('html').removeClass('menu-slide-active');
				$('.he-co-floating.cover').fadeOut();
			}
		}
		_slide.init();
	},
	products: function() {
		var _prod = {
			init: function() {
				$('.product-slider .pr-sl-carousel').flexslider({
					animation: "slide",
					controlNav: false,
					animationLoop: false,
					slideshow: false,
					itemWidth: 65,
					itemMargin: 10,
					asNavFor: '.pr-sl-main'
				});

				$('.product-slider .pr-sl-main').flexslider({
					animation: "slide",
					controlNav: false,
					nextText: '',
					prevText: '',
					animationLoop: false,
					slideshow: false,
					sync: ".pr-sl-carousel"
				});
			}
		}
		_prod.init();
	},
	popup: function() {
		var _popup = {
			auto: '',
			init: function() {
				$('[data-fancybox]').fancybox({
					animationEffect: 'zoom-in-out',
					transitionEffect: 'zoom-in-out',
					btnTpl: {
						smallBtn:'<button data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}">' + '<i class="icon-icon-close fz-16"></i>' + "</button>",
					}
				});

				_popup.auto = $('[data-popup="auto-popup"]').eq(0);
				$(window).load(function() {
					setTimeout(function() {
						if(_popup.auto.length == 1) {
							var _src = '#' + _popup.auto.attr('id');

							$.fancybox.open({
								src: _src,
								animationEffect: 'zoom-in-out',
								transitionEffect: 'zoom-in-out',
								btnTpl: {
									smallBtn:'<button data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}">' + '<i class="icon-icon-close fz-16"></i>' + "</button>",
								}
							});
						}
					}, 1000);
				});
			}
		}
		_popup.init();
	}
}
bp.init();

$(document).ready(function() {
	bp.ready();

	$('form.variations_form.cart').submit(function(){
		var _return = true;
		var label = '';
		$(this).find('.variations').each(function( index ) {
			if($(this).find('select').val() == '') {
				label = $(this).find('.label strong').text();
				_return = false;
				return false;
			}
		});
		if(!_return){
			alert(frontend_message_object.add_to_cart_error.replace("[label]", label));
			return false;
		}
	});

	function formatState (state) {
		if($(state.element).data('icon')){
			var $state = $(
				'<span class="img-flag" ><img src="' + $(state.element).data('icon') + '" /></span><span> ' + state.text + '</span>'
			);
		}else{
			var $state = $(
				'<span> ' + state.text + '</span>'
			);
		}
		return $state;
	};
	$(".select-country select").select2({
	  	width: '100%',
		templateResult: formatState,
		templateSelection: formatState,
		escapeMarkup: function(m) { return m; }
	});

	$( document.body ).on( 'change', '.select-country select', function() {
		$('.bp-preloader').css('opacity',.8).fadeIn();
		var country = $( this ).val();
		var current_url = window.location.href;
		jQuery.post(
		    frontend_ajax_object.ajaxurl, 
		    {
		        'action': 'update_country',
		        'current_url': current_url,
		        'country': country
		    }, 
		    function(redirect) {
				window.location.assign(redirect);
		    }
		);
	});

	$(document).on('change','.select-language select',function(){
		$('.bp-preloader').css('opacity',.8).fadeIn();
		location.assign($(this).val());
	});

	$(document).on('click','.select-language li:not(.disabled), .select-category li',function(){
		$('.bp-preloader').css('opacity',.8).fadeIn();
		location.assign($(this).data('href'));
	});

	$(document).on('click','.select-size li',function(){
		$('#pa_size').trigger('change');
	});
	$(document).on('click','.select-address-book li',function(){
		alert($('#address_book').val());
		$('#address_book').trigger('change');
	});

	// $(document).on('click','.single_add_to_cart_button',function(){
	// 	alert('Added');
	// });

	$(document).on('change','.coupon_code_tmp input',function(){
		$('#coupon_code').val($(this).val());
	});
	$(document).on('click','.coupon_code_tmp button',function(){
		$('.checkout_coupon').submit();
	});

	$(document).on('click', '.btn-plus', function(){
		var qty = $(this).parent().find('input');
			qty.val(parseInt(qty.val())+1);

			qty.change();
	});
	$(document).on('click', '.btn-minus', function(){
		var qty = $(this).parent().find('input');
		if(qty.val() > 1)
			qty.val(parseInt(qty.val())-1);

			qty.change();
	});

});

$(window).load(function() {
	bp.resize();
});

$(window).resize(function() {
	bp.resize();
});

