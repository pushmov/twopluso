<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Two_Plus_O
 */

get_header();
?>


	<section id="main-wrapper" class="push-top">
		<div class="container-fluid bg-gray-7 pt4 pb4 animate">
			<div class="container width-1">
				<div class="text-center">
					<h1 class="bp-title fz-50 fw-600 bp-tt"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
		<div class="container-fluid mt5 animate">
			<div class="container width-1">
				<!-- nav for desktop -->

				<?php
				$cat_args = array(
				    'hide_empty' => false,
				    'exclude'	 => 1
				);
				$product_categories = get_terms( 'press_release_cat', $cat_args );
				?>
				<?php if( !empty($product_categories) ): ?>
				<div class="bp-desktop2">
					<ul class="pr-re-nav fw-500 text-center">
						<li class="active"><a href="press-release"><?php _e('All','woocommerce'); ?></a></li>
						<?php foreach ($product_categories as $key => $category): ?>
							<li><a href="<?php echo get_term_link($category); ?>"><?php echo $category->name; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="bp-mobile2">
					<div class="bp-select cf-select fz-16 fw-600">
						<select name="" id="">
							<option value=""><?php _e('All','woocommerce'); ?></option>
							<?php foreach ($product_categories as $key => $category): ?>
								<option value="<?php echo get_term_link($category); ?>"><?php echo $category->name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<?php endif; ?>

				<div class="mt7">

				<?php if ( have_posts() ) : ?>

					<header class="page-header">
						<?php
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="archive-description">', '</div>' );
						?>
					</header><!-- .page-header -->

					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post(); ?>

						<ul group-height class="ho-insta-list col3 animate" anim-control="parent">
						   	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<li>
								<a class="bl-fe-set" href="<?php echo get_permalink(); ?>">
									<div class="bp-img wide"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>"></div>
									<div class="bf-se-col capt">
										<div class="in-co-par fz-16 fw-500 gh1">
											<p class="co-gray-1"><small><?php echo get_the_date('j m Y'); ?></small></p>
											<p><?php the_title(); ?></p>
										</div>
									</div>
								</a>
							</li>
							<?php endwhile; ?>
						</ul>

				<?php
					endwhile;

					the_posts_navigation();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>

					<!-- <div class="mt4 text-center animate">
						<a class="button btn-size-1 btn-black fz-14" href="#">More</a>
					</div> -->
				</div>
			</div>
		</div>
		<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
	</section>

<?php
get_sidebar();
get_footer();
