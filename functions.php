<?php
/**
 * Two Plus O functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Two_Plus_O
 */

if ( ! function_exists( 'two_plus_o_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function two_plus_o_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Two Plus O, use a find and replace
		 * to change 'two-plus-o' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'two-plus-o', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'top-menu' => esc_html__( 'Top Menu', 'two-plus-o' ),
			'primary-menu' => esc_html__( 'Primary Menu', 'two-plus-o' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'two_plus_o_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'two_plus_o_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function two_plus_o_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'two_plus_o_content_width', 640 );
}
add_action( 'after_setup_theme', 'two_plus_o_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function two_plus_o_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'two-plus-o' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'two-plus-o' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'two_plus_o_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function two_plus_o_scripts() {
	$version = time();

	// wp_enqueue_style( 'two-plus-o-style', get_stylesheet_uri() );
	wp_enqueue_style( 'two-plus-o-select2', get_template_directory_uri(). '/assets/css/select2.min.css', array(), $version );
	wp_enqueue_style( 'two-plus-o-style', get_template_directory_uri(). '/assets/css/style.css', array(), $version );
	wp_enqueue_style( 'two-plus-o-custom', get_template_directory_uri(). '/assets/css/custom.css', array(), $version );


	// wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/lib/jquery.min.js', array(), $version );
	wp_enqueue_script( 'two-plus-o-select2', get_template_directory_uri() . '/assets/js/lib/select2.min.js', array(), $version );
	wp_enqueue_script( 'two-plus-o-custom-script', get_template_directory_uri() . '/assets/js/custom.min.js', array(), $version, true );
    wp_deregister_script( 'wc-single-product' );
    wp_dequeue_script( 'wc-single-product' );
	wp_enqueue_script( 'wc-single-product', get_template_directory_uri() . '/assets/js/single-product.min.js', array( 'jquery' ), $version, true );

	wp_localize_script( 'two-plus-o-custom-script', 'frontend_ajax_object',
        array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        )
    );
	wp_localize_script( 'two-plus-o-custom-script', 'frontend_message_object',
        array( 
            'add_to_cart_error' => esc_html__( 'Please select product\'s [label] before adding this product to your cart.', 'woocommerce' )
        )
    );


	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }
}
add_action( 'wp_enqueue_scripts', 'two_plus_o_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function twopluso_startSession() {
    if(!session_id()) {
        session_start();
    }
}
add_action('init', 'twopluso_startSession', 1);

/*
 * Set post views count using post meta
 */
function setPostViews($postID) {
    $countKey = 'post_views_count';
    $count = get_post_meta($postID, $countKey, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $countKey);
        add_post_meta($postID, $countKey, '0');
    }else{
        $count++;
        update_post_meta($postID, $countKey, $count);
    }
}
function my_product_carousel_options($options) {
	$options['animation'] = 'slide';
	return $options;
}
add_filter("woocommerce_single_product_carousel_options", "my_product_carousel_options", 10);