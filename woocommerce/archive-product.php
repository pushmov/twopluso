<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

?>
<section id="main-wrapper" class="push-top">
	<div class="container-fluid mt8 mb10 mob-mt1 animate">
		<div class="container width-1">
			<!-- filters for desktop -->
			<div class="bp-desktop2">
				<div class="co-pr-ctrl d-flex in-co-par fz-16 fw-500">
					<div class="title"><?php _e('Filters','woocommerce'); ?>:</div>
					<div>
						<ul class="cp-ct-list">
							<?php
							$cat_args = array(
							    'hide_empty' => true,
							    'exclude'	 => 1
							);
							$product_categories = get_terms( 'product_cat', $cat_args );
							?>
							<?php if( !empty($product_categories) ): ?>
								<li>
									<div class="bp-select cp-select select-category">
										<select>
											<option value=""><?php _e('Categories','woocommerce'); ?></option>
											<?php foreach ($product_categories as $key => $category): ?>
												<option data-href="<?php echo get_term_link($category); ?>"><?php echo $category->name; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</li>
							<?php endif; ?>
							<!-- <li>
								<div class="bp-select cp-select">
									<select name="" id="">
										<option value="">Price</option>
										<option value="">Price 1</option>
										<option value="">Price 2</option>
										<option value="">Price 3</option>
									</select>
								</div>
							</li>
							<li>
								<div class="bp-select cp-select">
									<select name="" id="">
										<option value="">Color</option>
										<option value="">Color 1</option>
										<option value="">Color 2</option>
										<option value="">Color 3</option>
									</select>
								</div>
							</li> -->
							<?php
							$tag_args = array(
							    'hide_empty' => true,
							    'exclude'	 => 1
							);
							$product_tags = get_terms( 'product_tag', $tag_args );
							?>
							<?php if( !empty($product_tags) ): ?>
								<li>
									<div class="bp-select cp-select select-tag">
										<select>
											<option value=""><?php _e('Tags','woocommerce'); ?></option>
											<?php foreach ($product_tags as $key => $tag): ?>
												<option data-href="<?php echo get_term_link($tag); ?>"><?php echo $tag->name; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
			<!-- filters for mobile -->
			<div toggle-group="type4" class="bp-mobile2 in-co-par fz-16 fw-500">
				<div toggle-set>
					<div toggle-btn class="pr-fi-col title fw-600"><?php _e('Filters','woocommerce'); ?></div>
					<div toggle-main class="pr-fi-col main mt2">
						<ul class="cp-ct-list">

							<?php
							$cat_args = array(
							    'orderby'    => $orderby,
							    'order'      => 'asc',
							    'hide_empty' => true,
							    'exclude'	 => 1
							);
							$product_categories = get_terms( 'product_cat', $cat_args );
							?>
							<?php if( !empty($product_categories) ): ?>
								<li>
									<div class="bp-select cp-select select-category">
										<select>
											<option value=""><?php _e('Categories','woocommerce'); ?></option>
											<?php foreach ($product_categories as $key => $category): ?>
												<option data-href="<?php echo get_term_link($category); ?>"><?php echo $category->name; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</li>
							<?php endif; ?>
							<!-- <li>
								<div class="bp-select cp-select">
									<select name="" id="">
										<option value="">Price</option>
										<option value="">Price 1</option>
										<option value="">Price 2</option>
										<option value="">Price 3</option>
									</select>
								</div>
							</li>
							<li>
								<div class="bp-select cp-select">
									<select name="" id="">
										<option value="">Color</option>
										<option value="">Color 1</option>
										<option value="">Color 2</option>
										<option value="">Color 3</option>
									</select>
								</div>
							</li> -->
							<?php
							$tag_args = array(
							    'hide_empty' => true,
							    'exclude'	 => 1
							);
							$product_tags = get_terms( 'product_tag', $tag_args );
							?>
							<?php if( !empty($product_tags) ): ?>
								<li>
									<div class="bp-select cp-select select-tag">
										<select>
											<option value=""><?php _e('Tags','woocommerce'); ?></option>
											<?php foreach ($product_tags as $key => $tag): ?>
												<option data-href="<?php echo get_term_link($tag); ?>"><?php echo $tag->name; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="mt4">
				<?php if ( woocommerce_product_loop() ) {
					/**
					 * Hook: woocommerce_before_shop_loop.
					 *
					 * @hooked woocommerce_output_all_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );

					woocommerce_product_loop_start();

					if ( wc_get_loop_prop( 'total' ) ) {
						while ( have_posts() ) {
							the_post();
							
							/**
							 * Hook: woocommerce_shop_loop.
							 *
							 * @hooked WC_Structured_Data::generate_product_data() - 10
							 */
							do_action( 'woocommerce_shop_loop' );

							wc_get_template_part( 'content', 'product' );
						}
					}

					woocommerce_product_loop_end();

					/**
					 * Hook: woocommerce_after_shop_loop.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				} else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				}
				?>
			</div>
			<!-- <div class="mt4">
				<ul class="co-pr-pagination fz-14 animate">
					<li class="active"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
				</ul>
			</div> -->
		</div>
	</div>
</section>
<?php get_footer( 'shop' ); ?>


