<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>
<div class="woocommerce-variation-add-to-cart variations_button" style="overflow: hidden;">
	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
		<div style="overflow: hidden;" class="mt3">

			<?php
			do_action( 'woocommerce_before_add_to_cart_quantity' );

			woocommerce_quantity_input( array(
				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
				'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
			) );

			do_action( 'woocommerce_after_add_to_cart_quantity' );
			?>
		</div>

		<div class="mt3">
			<div class="form-group">
				<button type="submit" name="add-to-cart" class="single_add_to_cart_button alt button btn-black btn-size-2 btn-wide fw-600 bp-tt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
			</div>
		</div>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
</div>
<?php endif; ?>

<?php if(get_field('product_page_lists','options')): ?>
	<div class="mt3 mb3">
		<ul class="pr-co-list in-co-par fz-14">
			<?php foreach(get_field('product_page_lists','options') as $list): ?>
				<li>
					<span class="icon"><i class="icon-icon-check"></i></span><span>
						<?php if($list['text_bold']): ?>
							<strong><?php echo $list['text_bold']; ?></strong> 
						<?php endif; ?>
						<?php if($list['text']): ?>
							<?php echo $list['text']; ?>
						<?php endif; ?>
					</span>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>
