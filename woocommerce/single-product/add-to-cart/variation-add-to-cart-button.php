<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button" style="overflow: hidden;">
	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	<div style="overflow: hidden;">
		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );
		woocommerce_quantity_input( array(
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
		) );

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>
	</div>

	<div class="mt3">
		<div class="form-group">
			<button type="submit" class="single_add_to_cart_button_ alt button btn-black btn-size-2 btn-wide fw-600 bp-tt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		</div>
	</div>

	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
<?php if(get_field('product_page_lists','options')): ?>
	<div class="mt3">
		<ul class="pr-co-list in-co-par fz-14">
			<?php foreach(get_field('product_page_lists','options') as $list): ?>
				<li>
					<span class="icon"><i class="icon-icon-check"></i></span><span>
						<?php if($list['text_bold']): ?>
							<strong><?php echo $list['text_bold']; ?></strong> 
						<?php endif; ?>
						<?php if($list['text']): ?>
							<?php echo $list['text']; ?>
						<?php endif; ?>
					</span>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>