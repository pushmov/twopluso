<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<h4 class="bp-title fz-24 fw-600">
	<?php printf(__( 'Hello, <br>%1$s', 'woocommerce' ), esc_html( $current_user->display_name )); ?>
</h4>
<div class="in-co-par fz-14 fw-500 mt2">
	<p>
		<?php
			printf(
				__( 'Not %1$s? <a class="link-simple" href="%2$s">Log out</a>', 'woocommerce' ),
				'<strong>' . esc_html( $current_user->display_name ) . '</strong>',
				esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) )
			);
		?>
	</p>
	<div class="mt4">
		<p><?php
			printf(
				__( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a>, and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' ),
				esc_url( wc_get_endpoint_url( 'orders' ) ),
				esc_url( wc_get_endpoint_url( 'edit-address' ) ),
				esc_url( wc_get_endpoint_url( 'edit-account' ) )
			);
		?></p>
	</div>
	<div class="sh-ca-dir mob-col1 d-block d-sm-flex flex-center text-center text-center-sm fz-14 mt6">
		<a class="button btn-black btn-border btn-size-3 bp-tt fw-600" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e('Go Homepage','woocommerce'); ?></a>
		<a class="button btn-black btn-size-3 bp-tt fw-600" href="<?php echo wc_get_page_permalink( 'shop' ); ?>"><?php _e('Go Shopping','woocommerce'); ?></a>
	</div>
</div>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
