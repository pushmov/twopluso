<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$customer_id = get_current_user_id();

if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) {
	$get_addresses = apply_filters( 'woocommerce_my_account_get_addresses', array(
		'billing' => __( 'Billing address', 'woocommerce' ),
		'shipping' => __( 'Shipping address', 'woocommerce' ),
	), $customer_id );
} else {
	$get_addresses = apply_filters( 'woocommerce_my_account_get_addresses', array(
		'billing' => __( 'Billing address', 'woocommerce' ),
	), $customer_id );
}
?>

<div class="co-pr-col mb3">
	<h4 class="bp-title fz-24 fw-600"><?php echo __( 'Addresses', 'woocommerce' ); ?></h4>
	<div class="mt4">
		<div class="in-co-par fz-14">
			<p><?php echo apply_filters( 'woocommerce_my_account_my_address_description', __( 'The following addresses will be used on the checkout page by default.', 'woocommerce' ) ); ?></p>
		</div>
		<div class="row mt4">

		<?php foreach ( $get_addresses as $name => $title ) : ?>

			<div class="col-sm-6 mb3">
				<div class="d-flex flex-center justify-content-between fz-16 bp-img">
					<strong><?php echo $title; ?></strong>
					<div><a class="link-simple" href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', $name ) ); ?>"><i class="icon-icon-edit"></i></a></div>
				</div>
				<div class="in-co-par fz-14 mt1">
					<p>
					<?php
						$address = wc_get_account_formatted_address( $name );
						echo $address ? wp_kses_post( $address ) : esc_html_e( 'You have not set up this type of address yet.', 'woocommerce' );
					?>
					</p>
				</div>
			</div>

		<?php endforeach; ?>
		</div>
	</div>
</div>