<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="container-fluid mt5 mb10 animate">

	<div class="container width-1 mt4">
		<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

		<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

		<div group-height class="ch-lo-row bp-rel d-block d-md-flex justify-content-between flex-row flex-sm-row-reverse mob-text-center">
			<div class="cl-ro-col col-med mb7">

		<?php endif; ?>

				<?php
				woocommerce_login_form(
					array(
						'hidden'   => false
					)
				);
				?>

		<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

			</div>
			<div class="cl-ro-col cr-co-sep fz-16 mb7 mob-pull-img">
				<span><?php _e('OR','woocommerce');?></span>
			</div>
			<div class="cl-ro-col col-med mb7">
				<form method="post" class="woocommerce-form-- woocommerce-form-register-- register--" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

					<?php do_action( 'woocommerce_register_form_start' ); ?>

					<div class="gh1 cr-co-col col-top">
						<div class="sh-ca-title d-flex flex-center justify-content-center justify-content-sm-start bp-img">
							<div><i class="icon-icon-user-login icon-60"></i></div>
							<h3 class="bp-title fz-24 co-gray-1 fw-600"><?php _e('Register','woocommerce');?></h3>
							<?php echo ( $message ) ? wpautop( wptexturize( $message ) ) : ''; // @codingStandardsIgnoreLine ?>
						</div>
						<div class="con-form-group fz-14 mt4">

							<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
								<div class="form-group">
									<input type="text" class="form-control cf-input" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>"  placeholder="Your Username" /><?php // @codingStandardsIgnoreLine ?>
								</div>
							<?php endif; ?>
							<div class="form-group">
								<input type="email" class="form-control cf-input" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" placeholder="Your email address"  /><?php // @codingStandardsIgnoreLine ?>
							</div>
							<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
								<div class="form-group">
									<input type="password" class="form-control cf-input" name="password" id="reg_password" autocomplete="new-password" placeholder="Your Password" />
								</div>
							<?php endif; ?>

							<?php do_action( 'woocommerce_register_form' ); ?>
						</div>
					</div>
					<div class="cr-co-col col-bottom mt6">
						<div class="cr-co-ctrl d-flex flex-center justify-content-center justify-content-sm-start fz-14">
							<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
							<button type="submit" class="button btn-black btn-size-3 fw-600" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
						</div>
					</div>

					<?php do_action( 'woocommerce_register_form_end' ); ?>

				</form>
			</div>
		</div>

		<?php endif; ?>
		
		<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
	</div>
</div>