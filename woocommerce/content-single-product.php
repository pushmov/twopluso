<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
	<div class="row mb2 mob-mb5">
		<?php
			/**
			 * Hook: woocommerce_before_single_product_summary.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
		?>

		<div class="summary entry-summary col-md-6 mb5">
				<div class="container mob-no-padding width-7">
				<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );

					the_content();
				?>
			</div>
		</div>
	</div>
	<?php
	if ( function_exists( 'get_fields' ) ){
	  $fields = get_fields();
	  if($fields) extract($fields);
	}
	?>

	<?php if($description): ?>
		<?php foreach($description as $desc_key => $desc): ?>
			<div class="row row-simple d-flex flex-center mob-mb5 <?php echo $desc_key%2!=0?'flex-row-reverse':''; ?>">
				<div class="col-md-6">
					<div class="bp-img wide mob-pull-img">
						<img src="<?php echo $desc['image']['url']; ?>" alt="">
					</div>
				</div>
				<?php if($desc['description']): ?>
					<div class="col-md-6">
						<div class="container mob-no-padding width-7 mob-mt3">
							<div class="co-temp-par">
								<?php foreach($desc['description'] as $des): ?>
									<?php if($des['acf_fc_layout'] == 'title'): ?>
										<h5><?php echo $des['text']; ?></h5>
									<?php elseif($des['acf_fc_layout'] == 'text'): ?>
										<p><?php echo $des['text']; ?></p>
										<br>
									<?php elseif($des['acf_fc_layout'] == 'lists'): ?>
										<ul class="mgl-1">
											<?php foreach($des['list'] as $list): ?>
												<li><?php echo $list['text']; ?></li>
											<?php endforeach; ?>
										</ul>
										<br>
									<?php elseif($des['acf_fc_layout'] == 'icon'): ?>
										<ul class="pr-ins-list bp-img">
											<?php foreach($des['icon'] as $icon): ?>
												<li><img src="<?php echo $icon['image']['url']; ?>" width="23"></li>
											<?php endforeach; ?>
										</ul>
									<?php elseif($des['acf_fc_layout'] == 'image'): ?>
										<ul class="pr-ins-list bp-img">
											<img src="<?php echo $des['image']['url']; ?>" width="64">
										</ul>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
	<br>
	<br>
	<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
