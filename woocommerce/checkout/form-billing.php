<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $woocommerce;
/** @global WC_Checkout $checkout */

?>

<div class="container-fluid mt10 animate">
	<div class="container width-1">
		<div class="row">
			<div class="col-sm-8">
				<div class="sh-ca-title d-flex flex-center justify-content-center justify-content-sm-start bp-img fz-14">
					<div><i class="icon-icon-billing icon-60"></i></div>
					<div>
						<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

							<h3 class="bp-title fz-24 co-gray-1 fw-600 bp-tt"><?php _e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

						<?php else : ?>

							<h3 class="bp-title fz-24 co-gray-1 fw-600 bp-tt"><?php _e( 'Billing details', 'woocommerce' ); ?></h3>

						<?php endif; ?>
						<p><?php _e( 'All fields are required', 'woocommerce' ); ?></p>
					</div>
				</div>
				<style type="text/css">label{font-weight: 600;}</style>
				<div class="con-form-group fz-14 mt6 woocommerce-billing-fields">
					<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

						<?php
							$fields = $checkout->get_checkout_fields( 'billing' );

							foreach ( $fields as $key => $field ) {
								if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
									$field['country'] = $checkout->get_value( $field['country_field'] );
								}
								woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
							}
						?>
					<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>

					<?php if ( ! is_user_logged_in() && $checkout->is_registration_enabled() ) : ?>
						<div class="woocommerce-account-fields">
							<?php if ( ! $checkout->is_registration_required() ) : ?>

						<!-- 		<div class="form-group fz-16">
									<label class="cf-checkbox">
										<input id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true ) ?> type="checkbox" name="createaccount" value="1" />
										<span><?php _e( 'Create an account?', 'woocommerce' ); ?></span>
									</label>
								</div> -->
							<div class="form-group mt6">
								<label for=""><strong>Create an account (optional)</strong></label>
								<br>
								<label style="font-weight: 400;">Creating an account is not obligated</label>
							</div>
							<?php endif; ?>

							<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

							<?php if ( $checkout->get_checkout_fields( 'account' ) ) : ?>

								<div class="create-account--">
									<?php foreach ( $checkout->get_checkout_fields( 'account' ) as $key => $field ) : ?>
										<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
									<?php endforeach; ?>
									<div class="clear"></div>
								</div>

							<?php endif; ?>

							<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
						</div>
					<?php endif; ?>

				</div>

					

			</div>
			<div class="col-sm-4 mt1">
				<h3 class="bp-title fz-24 fw-600 bp-tt"><?php _e( 'Your order', 'woocommerce' ); ?></h3>
				<div class="co-sh-box bg-white mt4">
					<?php $cart_items = WC()->cart->get_cart(); ?>
					<table class="table-sg table-sc fz-14">
						<tbody>
							<tr class="text-left">
								<th><?php _e( 'Items', 'woocommerce' ); ?></th>
								<th><?php _e( 'Qty', 'woocommerce' ); ?></th>
								<th class="pdr-2 text-right"><?php _e( 'Total', 'woocommerce' ); ?></th>
							</tr>
							<?php
							foreach ( $cart_items as $cart_item_key => $cart_item ) {
								$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
								$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

								if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
									$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
									?>
									<tr>
										<td>
											<?php echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' ); ?>
										</td>
										<td>x<?php echo $cart_item['quantity']; ?></td>
										<td class="pdr-2 text-right">
											<?php
												echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
											?>
										</td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>
					</table>
					<div class="cs-bo-mor fz-14">
						<ul class="cb-mo-list">
							<li>
								<span class="title"><?php _e( 'Subtotal', 'woocommerce' ); ?> :</span>
								<span><strong><?php wc_cart_totals_subtotal_html(); ?></strong></span>
							</li>
							<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
								<li class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
									<span class="title"><?php _e( 'Coupon', 'woocommerce' ); ?> :</span>
									<span>
										<code><?php wc_cart_totals_coupon_label( $coupon ); ?></code>
										<strong><?php wc_cart_totals_coupon_html( $coupon ); ?></strong>
									</span>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<div class="mt3 fz-14">
					<a class="button btn-black btn-size-2 btn-wide fw-600" href="<?php echo wc_get_cart_url(); ?>"><?php _e( 'Back to Cart', 'woocommerce' ); ?></a>
				</div>
			</div>
		</div>
	</div>
</div>
