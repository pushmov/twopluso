<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="container-fluid mb10 animate">
	<div class="container width-1">
		<div class="sh-ca-title d-flex flex-center justify-content-center bp-img text-center">
			<div><i class="icon-icon-review-order icon-60"></i></div>
			<h3 class="bp-title fz-24 co-gray-1 fw-600 bp-tt"><?php _e( 'Review Order', 'woocommerce' ); ?></h3>
		</div>
		<!-- table for desktop -->
		<div class="mt8 bg-white bp-desktop2">
			<table class="table-sg table-sc in-co-par fz-14">
				<tbody>
					<tr>
						<th><?php _e( 'Product', 'woocommerce' ); ?></th>
						<th class="col-med"><?php _e( 'Description', 'woocommerce' ); ?></th>
						<th class="text-center"><?php _e( 'Color', 'woocommerce' ); ?></th>
						<th class="text-center" style="width: 123px;"><?php _e( 'Size', 'woocommerce' ); ?></th>
						<th class="text-center" style="width: 162px;"><?php _e( 'Qty', 'woocommerce' ); ?></th>
						<th class="pdr-2 text-right"><?php _e( 'Amount', 'woocommerce' ); ?></th>
					</tr>

					<?php
						do_action( 'woocommerce_review_order_before_cart_contents' );

						foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
							$_product	= apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

							if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
								?>

								<tr>
									<td>
										<div class="bp-img" style="max-width: 90px;">
											<?php
												$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
												echo $thumbnail;
											?>
										</div>
									</td>
									<td>
										<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ); ?>
									</td>
									<td class="text-center">
										<?php
											$taxonomy = 'pa_color';
											$meta = get_post_meta($cart_item['variation_id'], 'attribute_'.$taxonomy, true);
											$term = get_term_by('slug', $meta, $taxonomy);
											echo $term->name;
										?>
									</td>
									<td class="text-center">
										<?php
											$taxonomy = 'pa_size';
											$meta = get_post_meta($cart_item['variation_id'], 'attribute_'.$taxonomy, true);
											$term = get_term_by('slug', $meta, $taxonomy);
											echo $term->name;
										?>
									</td>
									<td class="text-center">
										<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity',sprintf( '%s', $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
									</td>
									<td class="pdr-2 text-right">
										<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
									</td>
								</tr>
								<?php
							}
						}

						do_action( 'woocommerce_review_order_after_cart_contents' );
					?>

				</tbody>
			</table>
		</div>

		<!-- table for mobile -->
		<div class="mt6 bg-white mob-pull-img bp-mobile2">
			<table class="table-sg table-sc in-co-par fz-14">
				<tbody>

					<?php
						do_action( 'woocommerce_review_order_before_cart_contents' );

						foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
							$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

							if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
								?>

								<tr>
									<td>
										<div class="sh-ca-dir d-flex flex-center">
											<div class="bp-img" style="max-width: 93px;">
												<?php
												$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
												echo $thumbnail;
											?>
											</div>
											<div>
												<p>
													<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ); ?>
												</p>
											</div>
										</div>
										<div class="sh-ca-dir d-flex flex-center justify-content-between">
											<div class="sh-ca-dir d-flex flex-center">
												<span style="text-align: center;border: 1px solid rgba(0,0,0,0.1);padding: 5px 10px;height: 34px;min-width: 93px;line-height: 22px;">
													<?php
														$taxonomy = 'pa_color';
														$meta = get_post_meta($cart_item['variation_id'], 'attribute_'.$taxonomy, true);
														$term = get_term_by('slug', $meta, $taxonomy);
														echo $term->name;
													?>,
													<?php
														$taxonomy = 'pa_size';
														$meta = get_post_meta($cart_item['variation_id'], 'attribute_'.$taxonomy, true);
														$term = get_term_by('slug', $meta, $taxonomy);
														echo $term->name;
													?>
												</span>
												<span style="text-align: center;border: 1px solid rgba(0,0,0,0.1);padding: 5px 10px;height: 34px;min-width: 93px;line-height: 22px;">
													x<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity',sprintf( '%s', $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
												</span>
												<span style="text-align: center;border: 1px solid rgba(0,0,0,0.1);padding: 5px 10px;height: 34px;min-width: 93px;line-height: 22px;">
													<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
												</span>
											</div>
										</div>
									</td>
								</tr>

								<?php
							}
						}

						do_action( 'woocommerce_review_order_after_cart_contents' );
					?>

				</tbody>
			</table>
		</div>

		<div class="row row-large mt5">
			<div class="col-sm-5 mb3 input-group cou-input-group fz-14 coupon_code_tmp">
				<div class="in-co-par fz-14">
					<p><?php esc_html_e( 'If you have a coupon code, please apply it below.', 'woocommerce' ); ?></p>
				</div>
				<div class="input-group cou-input-group fz-14">
					<input type="text" class="form-control" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" value="" />
					<div class="input-group-append">
						<button type="button" class="button" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_html_e( 'apply', 'woocommerce' ); ?></button>
					</div>
				</div>
				<ul class="woocommerce-error" role="alert" style="width: 100%;display: none;"></ul>
			</div>
			<div class="col-sm-7 mb3 sc-tot-list in-co-par fz-14 ">
				<ul class="woocommerce-checkout-review-order-table-total">
				</ul>
			</div>
		</div>
	</div>
</div>