<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/shipping-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<ul class="cb-mo-list woocommerce-checkout-review-order-table-total">
	<li>
		<span class="title"><?php _e( 'Subtotal', 'woocommerce' ); ?> :</span>
		<span><strong><?php wc_cart_totals_subtotal_html(); ?></strong></span>
	</li>
	<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
		<li class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
			<span class="title"><?php _e( 'Coupon', 'woocommerce' ); ?> :</span>
			<span>
				<code><?php wc_cart_totals_coupon_label( $coupon ); ?></code>
				<strong><?php wc_cart_totals_coupon_html( $coupon ); ?></strong>
			</span>
		</li>
	<?php endforeach; ?>
	<li class="flex-start">
		<span class="title"><?php _e( 'Shipping', 'woocommerce' ); ?> :</span>
		<span>
			<p>
				<?php
				$packages           = WC()->shipping->get_packages();
				$first              = true;

				foreach ( $packages as $i => $package ) {
					$chosen_method = isset( WC()->session->chosen_shipping_methods[ $i ] ) ? WC()->session->chosen_shipping_methods[ $i ] : '';
					$product_names = array();

					if ( count( $packages ) > 1 ) {
						foreach ( $package['contents'] as $item_id => $values ) {
							$product_names[ $item_id ] = $values['data']->get_name() . ' &times;' . $values['quantity'];
						}
						$product_names = apply_filters( 'woocommerce_shipping_package_details_array', $product_names, $package );
					}

					$available_methods = $package['rates'];
					$formatted_destination = WC()->countries->get_formatted_address( $package['destination'], ', ' );
					$has_calculated_shipping = WC()->customer->has_calculated_shipping();

					if ( $available_methods ) :
						foreach ( $available_methods as $method ) :
							if ( checked( $method->id, $chosen_method, false ) ) {
								echo wc_cart_totals_shipping_method_label( $method );
							}
						endforeach;
					elseif ( ! $has_calculated_shipping || ! $formatted_destination ) :
						esc_html_e( 'Enter your address to view shipping options.', 'woocommerce' );
					elseif ( ! is_cart() ) :
						echo wp_kses_post( apply_filters( 'woocommerce_no_shipping_available_html', __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) );
					else :
						// Translators: $s shipping destination.
						echo wp_kses_post( apply_filters( 'woocommerce_cart_no_shipping_available_html', sprintf( esc_html__( 'No shipping options were found for %s.', 'woocommerce' ) . ' ', '<strong>' . esc_html( $formatted_destination ) . '</strong>' ) ) );
						$calculator_text = __( 'Enter a different address', 'woocommerce' );
					endif;

				}

				?>
			</p>
		</span>
	</li>
	<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
		<li class="fee">
			<span class="title"><?php echo esc_html( $fee->name ); ?> :</span>
			<span><strong><?php wc_cart_totals_fee_html( $fee ); ?></strong></span>
		</li>
	<?php endforeach; ?>

	<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
		<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
			<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
				<li class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
					<span class="title"><?php echo esc_html( $tax->label ); ?> :</span>
					<span><strong><?php echo wp_kses_post( $tax->formatted_amount ); ?></strong></span>
				</li>
			<?php endforeach; ?>
		<?php else : ?>
			<li class="tax-total">
				<span class="title"><?php echo esc_html( WC()->countries->tax_or_vat() ); ?> :</span>
				<span><strong><?php wc_cart_totals_taxes_total_html(); ?></strong></span>
			</li>
		<?php endif; ?>
	<?php endif; ?>
	<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>
	<li class="flex-center">
		<span class="title"><?php _e( 'Total', 'woocommerce' ); ?> :</span>
		<span>
			<h4 class="bp-title fz-24 fw-500 co-blue-1"><?php wc_cart_totals_order_total_html(); ?></h4>
		</span>
	</li>
	<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>
</ul>