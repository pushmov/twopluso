<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="container-fluid mt10 mb10 animate">
	<div class="container width-1">
		<div class="row">
			<div class="col-sm-8">
				<div class="sh-ca-title d-flex flex-center justify-content-center justify-content-sm-start bp-img">
					<div><i class="icon-icon-shipping icon-60"></i></div>
					<h3 class="bp-title fz-24 co-gray-1 fw-600 bp-tt"><?php _e( 'Shipping Method', 'woocommerce' ); ?></h3>
				</div>
				<div class="mt4">
					<h4 class="fz-16 fw-600"><?php _e( 'Select shipping', 'woocommerce' ); ?></h4>

					<div class="shop_table woocommerce-checkout-review-order-table">

					</div>

				</div>
				<div class="mt6">

					<div class="woocommerce-billing-fields-clone">

					</div>


					<div class="woocommerce-shipping-fields">


						<?php if ( true === WC()->cart->needs_shipping_address() ) : ?>


							<div class="shipping_address">

								<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

								<div class="woocommerce-shipping-fields__field-wrapper con-form-group fz-14">
									<?php
										$fields = $checkout->get_checkout_fields( 'shipping' );

										foreach ( $fields as $key => $field ) {
											if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
												$field['country'] = $checkout->get_value( $field['country_field'] );
											}
											woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
										}
									?>
								</div>

								<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

							</div>

							<h3 id="ship-to-billing-address" style="display: none;">
								<div class="form-group">
									<label class="cf-checkbox">
										<input id="ship-to-billing-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' !== get_option( 'woocommerce_ship_to_destination' ) ? 0 : 1 ), 0 ); ?> type="checkbox" name="ship_to_billing_address" value="1" /> <span class="pb-title fz-24 fw-600"><?php _e( 'Ship to a billing address', 'woocommerce' ); ?></span>
									</label>
								</div>
							</h3>
							<h3 id="ship-to-different-address" style="display: none;">
								<div class="form-group">
									<label class="cf-checkbox">
										<input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" /> <span class="pb-title fz-24 fw-600"><?php _e( 'Ship to a different addresses', 'woocommerce' ); ?></span>
									</label>
								</div>
							</h3>

						<?php endif; ?>
					</div>

					<div class="woocommerce-additional-fields con-form-group fz-14">
						<?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

						<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', 'yes' === get_option( 'woocommerce_enable_order_comments', 'yes' ) ) ) : ?>

							<?php if ( ! WC()->cart->needs_shipping() || wc_ship_to_billing_address_only() ) : ?>

								<h3><?php _e( 'Additional information', 'woocommerce' ); ?></h3>

							<?php endif; ?>

							<div class="woocommerce-additional-fields__field-wrapper">
								<?php foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) : ?>
									<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
								<?php endforeach; ?>
							</div>

						<?php endif; ?>

						<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
					</div>

				</div>
			</div>
			<div class="col-sm-4 mt2">
				<h3 class="bp-title fz-24 fw-600 bp-tt"><?php _e( 'Your order', 'woocommerce' ); ?></h3>
				<div class="co-sh-box bg-white mt4">
					<?php $cart_items = WC()->cart->get_cart(); ?>
					<table class="table-sg table-sc fz-14">
						<tbody>
							<tr class="text-left">
								<th><?php _e( 'Items', 'woocommerce' ); ?></th>
								<th><?php _e( 'Qty', 'woocommerce' ); ?></th>
								<th class="pdr-2 text-right"><?php _e( 'Total', 'woocommerce' ); ?></th>
							</tr>

							<?php
							foreach ( $cart_items as $cart_item_key => $cart_item ) {
								$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
								$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

								if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
									$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
									?>
									<tr>
										<td>
											<?php echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' ); ?>
										</td>
										<td>x<?php echo $cart_item['quantity']; ?></td>
										<td class="pdr-2 text-right">
											<?php
												echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
											?>
										</td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>
					</table>
					<div class="cs-bo-mor fz-14">
						<ul class="cb-mo-list woocommerce-checkout-review-order-table-total"></ul>
					</div>
				</div>
				<div class="mt3 fz-14">
					<a class="button btn-black btn-size-2 btn-wide fw-600" href="<?php echo wc_get_cart_url(); ?>"><?php _e( 'Back to Cart', 'woocommerce' ); ?></a>
				</div>
			</div>
		</div>
	</div>
</div>