<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="container-fluid mt8 mb10 mob-mt3 animate">
	<?php do_action( 'woocommerce_before_cart' ); ?>
	<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>
		<div class="container width-1">
			<h1 class="bp-title fz-50 fw-600 bp-tt"><?php _e('Your <br>Shopping cart'); ?></h1>
			<!-- table for desktop -->
			<div class="mt5 bg-white bp-desktop2">
				<table class="table-sc in-co-par fz-14">
					<tbody>
						<tr>
							<th colspan="2"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
							<th class="text-center" style="width: 123px;"><?php esc_html_e( 'Color', 'woocommerce' ); ?></th>
							<th class="text-center" style="width: 123px;"><?php esc_html_e( 'Size', 'woocommerce' ); ?></th>
							<th class="text-center" style="width: 162px;"><?php esc_html_e( 'Qty', 'woocommerce' ); ?></th>
							<th class="text-right"><?php esc_html_e( 'Amount', 'woocommerce' ); ?></th>
							<th class="product-remove" style="width: 70px;">&nbsp;</th>
						</tr>

						<?php do_action( 'woocommerce_before_cart_contents' ); ?>

						<?php
						foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
							$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
							$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

							if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
								$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
								?>
								<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

									<td class="product-thumbnail">
										<div class="bp-img" style="max-width: 90px;">
											<?php
											$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

											if ( ! $product_permalink ) {
												echo $thumbnail; // PHPCS: XSS ok.
											} else {
												printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
											}
											?>
										</div>
									</td>
									<td class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
									<?php
										echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );

										do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

										// Meta data.
										// echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

										// Backorder notification.
										if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
											echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
										}
									?>
									</td>
									<td class="text-center">
										<?php
											$taxonomy = 'pa_color';
											$meta = get_post_meta($cart_item['variation_id'], 'attribute_'.$taxonomy, true);
											$term = get_term_by('slug', $meta, $taxonomy);
											echo $term->name;
										?>
									</td>
									<td class="text-center">
										<?php
											$taxonomy = 'pa_size';
											$meta = get_post_meta($cart_item['variation_id'], 'attribute_'.$taxonomy, true);
											$term = get_term_by('slug', $meta, $taxonomy);
											echo $term->name;
										?>
									</td>
									<td class="product-quantity text-center" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
									<?php
									if ( $_product->is_sold_individually() ) {
										$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
									} else {
										$product_quantity = woocommerce_quantity_input( array(
											'input_name'   => "cart[{$cart_item_key}][qty]",
											'input_value'  => $cart_item['quantity'],
											'max_value'    => $_product->get_max_purchase_quantity(),
											'min_value'    => '0',
											'product_name' => $_product->get_name(),
										), $_product, false );
									}

									echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
									?>
									</td>

									<td class="product-subtotal text-right" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
										<?php
											echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
										?>
									</td>

									<td class="product-remove text-right">
										<?php
											// @codingStandardsIgnoreLine
											echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
												'<a href="%s" class="link-delete fz-16" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="icon-icon-delete"></i></a>',
												esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
												__( 'Remove this item', 'woocommerce' ),
												esc_attr( $product_id ),
												esc_attr( $_product->get_sku() )
											), $cart_item_key );
										?>
									</td>
								</tr>
								<?php
							}
						}
						?>
						<tr>
							<td colspan="5" class="text-right"><?php esc_html_e( 'Sub Total', 'woocommerce' ); ?></td>
							<td class="product-subtotal text-right">
								<strong><?php wc_cart_totals_subtotal_html(); ?></strong>
							</td>
							<td></td>
						</tr>
						<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
							<tr>
								<td colspan="5" class="text-right" rowspan="<?php count(WC()->cart->get_coupons()); ?>" >
									<?php esc_html_e( 'Coupon', 'woocommerce' ); ?>
								</td>
								<td colspan="2" class="text-right">
									<code><?php wc_cart_totals_coupon_label( $coupon ); ?></code>
									<strong><?php wc_cart_totals_coupon_html( $coupon ); ?></strong>
								</td>
							</tr>
						<?php endforeach; ?>
						<?php if(count(WC()->cart->get_coupons()) > 0 ): ?>
							<tr>
								<td colspan="4" ></td>
								<td class="text-right"><?php esc_html_e( 'Shipping', 'woocommerce' ); ?></td>
								<td colspan="2" class="text-right">

									<?php
									$packages           = WC()->shipping->get_packages();
									$first              = true;

									foreach ( $packages as $i => $package ) {
										$chosen_method = isset( WC()->session->chosen_shipping_methods[ $i ] ) ? WC()->session->chosen_shipping_methods[ $i ] : '';
										$product_names = array();

										if ( count( $packages ) > 1 ) {
											foreach ( $package['contents'] as $item_id => $values ) {
												$product_names[ $item_id ] = $values['data']->get_name() . ' &times;' . $values['quantity'];
											}
											$product_names = apply_filters( 'woocommerce_shipping_package_details_array', $product_names, $package );
										}

										$available_methods = $package['rates'];
										$formatted_destination = WC()->countries->get_formatted_address( $package['destination'], ', ' );
										$has_calculated_shipping = WC()->customer->has_calculated_shipping();

										if ( $available_methods ) :
											foreach ( $available_methods as $method ) :
												if ( checked( $method->id, $chosen_method, false ) ) {
													echo wc_cart_totals_shipping_method_label( $method );
												}
											endforeach;
										elseif ( ! $has_calculated_shipping || ! $formatted_destination ) :
											esc_html_e( 'Enter your address to view shipping options.', 'woocommerce' );
										elseif ( ! is_cart() ) :
											echo wp_kses_post( apply_filters( 'woocommerce_no_shipping_available_html', __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) );
										else :
											// Translators: $s shipping destination.
											echo wp_kses_post( apply_filters( 'woocommerce_cart_no_shipping_available_html', sprintf( esc_html__( 'No shipping options were found for %s.', 'woocommerce' ) . ' ', '<strong>' . esc_html( $formatted_destination ) . '</strong>' ) ) );
											$calculator_text = __( 'Enter a different address', 'woocommerce' );
										endif;

									}

									?>
								</td>
							</tr>
							<tr>
								<td colspan="5" class="text-right"><?php esc_html_e( 'Total', 'woocommerce' ); ?></td>
								<td class="text-right">
									<strong><?php wc_cart_totals_order_total_html(); ?></strong>
								</td>
								<td></td>
							</tr>
						<?php endif; ?>
						<?php do_action( 'woocommerce_cart_contents' ); ?>
					</tbody>
				</table>
			</div>
			<!-- table for mobile -->
			<div class="mt5 bg-white mob-pull-img bp-mobile2">
				<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents table-sg table-sc in-co-par fz-14">
					<tbody>

						<?php
						foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
							$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
							$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
							?>
						<tr>
							<td>
								<div class="sh-ca-dir d-flex flex-center">
									<div class="bp-img" style="max-width: 93px;">
										<?php
										$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

										if ( ! $product_permalink ) {
											echo $thumbnail; // PHPCS: XSS ok.
										} else {
											printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
										}
										?>
									</div>
									<div>
										<p>
											<?php
												echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );

											do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

											// Meta data.
											echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

											// Backorder notification.
											if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
												echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
											}
											?> <br>
											<?php
												/* PRICE PER PIECE OF PRODUCT
												echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
												*/
											?>
											<?php
												/*SUBTOTAL OF PRODUCT*/
												echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
											?>
										</p>
									</div>
								</div>
								<div class="sh-ca-dir d-flex flex-center justify-content-between">
									<div class="sh-ca-dir d-flex flex-center">
										<span style="text-align: center;border: 1px solid rgba(0,0,0,0.1);padding: 5px 10px;height: 34px;min-width: 93px;line-height: 22px;">
											<?php
												$taxonomy = 'pa_color';
												$meta = get_post_meta($cart_item['variation_id'], 'attribute_'.$taxonomy, true);
												$term = get_term_by('slug', $meta, $taxonomy);
												echo $term->name;
											?>,
											<?php
												$taxonomy = 'pa_size';
												$meta = get_post_meta($cart_item['variation_id'], 'attribute_'.$taxonomy, true);
												$term = get_term_by('slug', $meta, $taxonomy);
												echo $term->name;
											?>
										</span>
										<?php 
										if ( $_product->is_sold_individually() ) {
											$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
										} else {
											$product_quantity = woocommerce_quantity_input( array(
												'input_name'   => "cart[{$cart_item_key}][qty]",
												'input_value'  => $cart_item['quantity'],
												'max_value'    => $_product->get_max_purchase_quantity(),
												'min_value'    => '0',
												'product_name' => $_product->get_name(),
											), $_product, false );
										}
										echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
										?>
									</div>
									<?php
										// @codingStandardsIgnoreLine
										echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
											'<a href="%s" class="link-delete fz-20" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="icon-icon-delete"></i></a>',
											esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
											__( 'Remove this item', 'woocommerce' ),
											esc_attr( $product_id ),
											esc_attr( $_product->get_sku() )
										), $cart_item_key );
									?>
								</div>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</div>

			<div group-height class="row row-large mt5 actions">
				<div class="col-sm-5">
					<div class="gh1 in-co-par fz-14 mb1">
						<p>If you have a coupon code, please apply it below</p>
					</div>
					<?php if ( wc_coupons_enabled() ) { ?>
						<div class="coupon input-group cou-input-group fz-14">
							<input type="text" name="coupon_code" class="form-control" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" />
							<div class="input-group-append">
								<button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'apply', 'woocommerce' ); ?></button>
							</div>
							<?php do_action( 'woocommerce_cart_coupon' ); ?>
						</div>
					<?php } ?>
				</div>
				<div class="col-sm-7">
					<div class="gh1 in-co-par fz-14 mb1"></div>
					<!-- buttons for desktop -->
					<div class="bp-desktop2">
						<div class="cr-co-ctrl d-flex flex-center justify-content-end fz-14 fw-600">
							<a class="link-simple" href="<?php echo wc_get_page_permalink( 'shop' ); ?>"><?php esc_attr_e( 'Continue Shopping', 'woocommerce' ); ?></a>
							<button type="submit" class="button btn-black btn-border btn-size-3 fw-600" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>
						</div>
					</div>
					<!-- buttons for mobile -->
					<div class="bp-mobile2">
						<div class="cr-co-ctrl mob-ctrl text-center fz-14 fw-600">
							<button type="submit" class="button btn-black btn-border btn-size-3 fw-600" id="update_cart_m" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>
							
							<div>
							<a class="link-simple" href="<?php echo wc_get_page_permalink( 'shop' ); ?>"><?php esc_attr_e( 'Continue Shopping', 'woocommerce' ); ?></a>
							</div>
						</div>	
						<script>
							//quick fix to keep the update cart button enabled. there is a code somewhere that disables the said button.
							window.setInterval(function(){
								document.getElementById("update_cart_m").disabled = false;
							}, 1000);
						</script>
					</div>

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>

					<?php do_action( 'woocommerce_after_cart_contents' ); ?>
				</div>
			</div>
			<?php do_action( 'woocommerce_after_cart_table' ); ?>
		</div>
	</form>
</div>
<div class="container-fluid mt10 mb10 animate">
	<div class="container width-5">
		<?php
    		do_action( 'woocommerce_proceed_to_checkout' );
		?>
	</div>
</div>
<?php
	/**
	 * Cart collaterals hook.
	 *
	 * @hooked woocommerce_cross_sell_display
	 * @hooked woocommerce_cart_totals - 10
	 */
	do_action( 'woocommerce_cart_collaterals' );
?>

<?php do_action( 'woocommerce_after_cart' ); ?>
