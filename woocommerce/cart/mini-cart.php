<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_mini_cart' ); ?>

<?php if ( ! WC()->cart->is_empty() ) : ?>

	<div class="hs-bo-col main">
		<div class="bp-desktop">
			<?php $cart_items = WC()->cart->get_cart(); ?>
			<?php if(array_exists($cart_items)): ?>
				<table class="table-sg in-co-par fz-14 fw-500 woocommerce-mini-cart cart_list product_list_widget <?php echo esc_attr( $args['list_class'] ); ?>">
					<?php
					do_action( 'woocommerce_before_mini_cart_contents' );
					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
						$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
							?>
							<tr>
								<td class="thumbnail-cart-popup"><?php echo apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key ); ?></td>
								<td>
									<p>
										<?php echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' ); ?>
										<br>
										<small><?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); ?></small>
									</p>
								</td>
								<td class="pdr-2 text-center" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
								x<?php echo $cart_item['quantity']; ?>
								</td>
								<td class="product-remove text-right">
									<?php
										// @codingStandardsIgnoreLine
										echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
											'<a href="%s" class="link-delete fz-14" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="icon-icon-close"></i></a>',
											esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
											__( 'Remove this item', 'woocommerce' ),
											esc_attr( $product_id ),
											esc_attr( $_product->get_sku() )
										), $cart_item_key );
									?>
								</td>
							</tr>
							<?php
						}
					}
					do_action( 'woocommerce_mini_cart_contents' );
					?>

				</table>
			<?php endif; ?>
		</div>
		<div class="bp-mobile">
			<table class="table-sg in-co-par fz-14">
				<?php echo esc_attr( $args['list_class'] ); ?>
				<?php
				do_action( 'woocommerce_before_mini_cart_contents' );
				foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
					$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
					$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

					if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
						$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
						?>
						<tr>
							<td>
								<div class="sh-ca-dir d-flex flex-center">
									<div class="thumbnail-cart-popup">
										<?php echo apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key ); ?>
									</div>
									<div>
										<p>
										<?php
												echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );

												do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );
												// Meta data.
												echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

												// Backorder notification.
												if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
													echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
												}
												?> <br>
											<small>
												<?php
													echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
												?>
											</small>
										</p>
									</div>
								</div>
								<div class="sh-ca-dir d-flex flex-center justify-content-between mt1">
									<span style="text-align: center;border: 1px solid rgba(0,0,0,0.1);padding: 5px 10px;height: 34px;min-width: 93px;line-height: 22px;">
										<?php
											$taxonomy = 'pa_color';
											$meta = get_post_meta($cart_item['variation_id'], 'attribute_'.$taxonomy, true);
											$term = get_term_by('slug', $meta, $taxonomy);
											echo $term->name;
										?>,
										<?php
											$taxonomy = 'pa_size';
											$meta = get_post_meta($cart_item['variation_id'], 'attribute_'.$taxonomy, true);
											$term = get_term_by('slug', $meta, $taxonomy);
											echo $term->name;
										?>
									</span>
									<span style="text-align: center;border: 1px solid rgba(0,0,0,0.1);padding: 5px 10px;height: 34px;min-width: 93px;line-height: 22px;">
										x<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity',sprintf( '%s', $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
									</span>
									<?php
										echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
											'<a href="%s" class="link-delete fz-14" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="icon-icon-close"></i></a>',
											esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
											__( 'Remove this item', 'woocommerce' ),
											esc_attr( $product_id ),
											esc_attr( $_product->get_sku() )
										), $cart_item_key );
									?>
								</div>
							</td>
						</tr>

						<?php
					}
				}
				do_action( 'woocommerce_mini_cart_contents' );
				?>

			</table>
		</div>
	</div>
	<div class="hs-bo-col mt3">
		<div class="sh-ca-dir in-co-par fz-14 d-flex flex-center justify-content-between">
			<div class="sh-ca-dir d-flex flex-center">
				<div class="hb-co-col short"></div>
				<div>
					<p class="woocommerce-mini-cart__total total">
						<span class="font-b"><?php _e( 'Subtotal', 'woocommerce' ); ?></span><br>
						<strong class="fz-16">
							<?php echo WC()->cart->get_cart_subtotal(); ?>
						</strong>
					</p>
				</div>
			</div>
			<a class="button btn-black btn-size-1 fw-600" href="<?php echo wc_get_page_permalink( 'cart' ); ?>"><?php echo _e('Checkout','woocommerce'); ?></a>
		</div>
	</div>


<?php else : ?>

	<p class="woocommerce-mini-cart__empty-message"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></p>

<?php endif; ?>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
