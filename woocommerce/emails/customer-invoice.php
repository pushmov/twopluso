<?php
/**
 * Customer invoice email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-invoice.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Executes the e-mail header.
 *
 * @hooked WC_Emails::email_header() Output the email header
 */
// do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
		<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
	</head>
	<body marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'?>">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
				<tr>
					<td align="center" valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="700" id="template_container">
							<tr>
								<td align="center" valign="top">
									<!-- Body -->
									<table border="0" cellpadding="0" cellspacing="0" width="700" id="template_body">
										<tr>
											<td valign="top" id="body_content">
												<!-- Content -->
												<table border="0" cellpadding="20" cellspacing="0" width="100%">
													<tr>
														<td valign="top">
															<div id="body_content_inner">
<table width="100%">
	<tr>
		<td width="70%">
			<?php
				if ( $img = get_option( 'woocommerce_email_header_image' ) ) {
					echo '<p style="margin:0;max-width:200px;text-align:left;"><img src="' . esc_url( $img ) . '" alt="' . get_bloginfo( 'name', 'display' ) . '" style="width:100%;" /></p>';
				}
			?>
		</td>
		<td style="vertical-align: middle;">
			<?php
				$store_address     = get_option( 'woocommerce_store_address' );
				$store_address_2   = get_option( 'woocommerce_store_address_2' );
				$store_city        = get_option( 'woocommerce_store_city' );
				$store_postcode    = get_option( 'woocommerce_store_postcode' );

				// The country/state
				$store_raw_country = get_option( 'woocommerce_default_country' );

				// Split the country/state
				$split_country = explode( ":", $store_raw_country );

				// Country and state separated:
				$store_country = $split_country[0];
				$store_state   = $split_country[1];

				echo $store_address . "<br />";
				echo ( $store_address_2 ) ? $store_address_2 . "<br />" : '';
				echo $store_city . ', ' . $store_state . ' ' . $store_postcode . "<br />";
				echo $store_country;
			?>
		</td>
	</tr>
	<tr>
		<td>
			<h3 style="text-align: left;"><?php _e('INVOICE','woocommerce'); ?></h3>
			<?php $address    = $order->get_formatted_billing_address(); ?>
			<?php echo wp_kses_post( $address ? $address : esc_html__( 'N/A', 'woocommerce' ) ); ?>
			<?php if ( $order->get_billing_phone() ) : ?>
				<br/><?php echo esc_html( $order->get_billing_phone() ); ?>
			<?php endif; ?>
			<?php if ( $order->get_billing_email() ) : ?>
				<br/><?php echo esc_html( $order->get_billing_email() ); ?>
			<?php endif; ?>
		</td>
		<td style="vertical-align: top;">

			<?php _e('Order Number.','woocommerce'); ?>: <?php _e( $order->get_order_number() ); ?>
			<br>
			<?php _e('Order Date','woocommerce'); ?>: <?php _e( wc_format_datetime( $order->get_date_created() ) ); ?>
			<br>
			<?php $totals = $order->get_order_item_totals(); ?>
			<?php _e('Payment Method','woocommerce'); ?>: <?php _e($totals['payment_method']['value'],'woocommerce'); ?>

		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%;">
				<tr>
					<th class="product"><?php _e('Product Code', 'woocommerce' ); ?></th>
					<th class="product"><?php _e('Product description', 'woocommerce' ); ?></th>
					<th align="center" class="product"><?php _e('Color,Size', 'woocommerce' ); ?></th>
					<th align="right" class="quantity"><?php _e('Quantity', 'woocommerce' ); ?></th>
					<th align="right" class="price"><?php _e('Subtotal', 'woocommerce' ); ?></th>
				</tr>

				<?php 
				$text_align  = is_rtl() ? 'right' : 'left';
				$margin_side = is_rtl() ? 'left' : 'right';

				foreach ( $order->get_items() as $item_id => $item ) :
					$product       = $item->get_product();
					$sku           = '';
					$purchase_note = '';
					$image         = '';

					if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
						continue;
					}

					if ( is_object( $product ) ) {
						$sku           = $product->get_sku();
						$purchase_note = $product->get_purchase_note();
						$image         = $product->get_image( $image_size );
					}

					?>
					<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
						<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word; width: 1%;">
							<?php echo $sku; ?>
						</td>
						<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;">
						<?php
							echo wp_kses_post( get_the_title( $item['product_id'] ) );
						?>
						</td>
						<td class="td" style="text-align:center; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;">
						<?php
							$p_name = explode('-', $item['name']);
							echo isset($p_name[count($p_name)-1]) ? $p_name[count($p_name)-1] : 'n/a';
						?>
						<?php
						// allow other plugins to add additional product information here.
						// do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, $plain_text );

						// wc_display_item_meta(
						// 	$item,
						// 	array(
						// 		'label_before' => '<strong class="wc-item-meta-label" style="float: ' . esc_attr( $text_align ) . '; margin-' . esc_attr( $margin_side ) . ': .25em; clear: both">',
						// 	)
						// );

						// allow other plugins to add additional product information here.
						// do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, $plain_text );

						?>
						</td>
						<td class="td" style="text-align:right; ?>; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;">
						<?php echo wp_kses_post( apply_filters( 'woocommerce_email_order_item_quantity', $item->get_quantity(), $item ) ); ?>
						<?php
						// SKU.
						if ( $show_sku && $sku ) {
							echo wp_kses_post( ' (#' . $sku . ')' );
						}
						?>
						</td>
						<td class="td" style="text-align:right; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
							<?php echo wp_kses_post( $order->get_formatted_line_subtotal( $item ) ); ?>
						</td>
					</tr>
					<?php

					if ( $show_purchase_note && $purchase_note ) {
						?>
						<tr>
							<td colspan="3" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
								<?php
								echo wp_kses_post( wpautop( do_shortcode( $purchase_note ) ) );
								?>
							</td>
						</tr>
						<?php
					}
					?>

				<?php endforeach; ?>
				<?php

				if ( $totals ) {
					$i = 0;
					foreach ( $totals as $total_key => $total ) {
						if($total_key == 'payment_method') continue;
						$i++;
						?>
						<tr>
							<th class="td" scope="row" colspan="3" style="text-align:right; vertical-align: top; <?php echo ( 1 === $i ) ? 'border-top: 2px solid #e5e5e5;' : ''; ?>"><?php echo wp_kses_post( $total['label'] ); ?></th>
							<td class="td" colspan="2" style="text-align:right; <?php echo ( 1 === $i ) ? 'border-top: 2px solid #e5e5e5;' : ''; ?> max-width: 80px;">
								<?php echo wp_kses_post( $total['value'] ); ?></td>
						</tr>
						<?php
					}
				}
				?>
			</table>
		</td>
	</tr>


</table>
<br>
<?php



/**
 * Hook for the woocommerce_email_order_details.
 *
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
// do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Hook for the woocommerce_email_order_meta.
 *
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * Hook for woocommerce_email_customer_details.
 *
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
// do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

?>
<p>
<?php _e('If you have any questions, please contact our customer service through','woocommerce'); ?> <a href="<?php echo esc_url( home_url( '/contact' ) );?>"><?php echo esc_url( home_url( '/contact' ) );?></a>
<p>
<?php
_e('E.&O.E.','woocommerce');

/**
 * Executes the email footer.
 *
 * @hooked WC_Emails::email_footer() Output the email footer
 */
// do_action( 'woocommerce_email_footer', $email );
?>
</p>
