<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>


<span><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></span><p></p>

<span><?php _e('Welcome to 2+O™ (twopluso.com),','woocommerce'); ?></span><p></p>
<span><?php _e('To sign in to our site, please use these credentials during checkout or on log in to My Account:','woocommerce'); ?></span><p></p>
<span><?php printf( esc_html__( 'Email: %s', 'woocommerce' ), '<strong>' . esc_html( $email->user_email ) . '</strong>' ); ?></span><p></p>
<span><?php printf( esc_html__( 'Password: %s', 'woocommerce' ), '<i>' . esc_html( 'Your account password' ) . '</i>' ); ?></span><p></p>

<span><?php _e('Forgot your account password? Click here to reset it.','woocommerce'); ?></span><p></p>
<span><?php _e('When you sign in to your account, you will be able to:','woocommerce'); ?></span><p></p>
<ul>
	<li><?php _e('Complete the checkout process faster','woocommerce'); ?></li>
	<li><?php _e('Check the status of your order/s','woocommerce'); ?></li>
	<li><?php _e('View past orders','woocommerce'); ?></li>
	<li><?php _e('Store alternative addresses (for shipping to multiple family members and friends)','woocommerce'); ?></li>
</ul>

<span><?php _e('Thank you, 2+O™','woocommerce'); ?></span><p></p>

<?php
do_action( 'woocommerce_email_footer', $email );
