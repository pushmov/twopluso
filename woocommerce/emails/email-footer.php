<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
															</div>
														</td>
													</tr>
												</table>
												<!-- End Content -->
											</td>
										</tr>
									</table>
									<!-- End Body -->
								</td>
							</tr>
							<tr>
								<td align="center" valign="top">
									<!-- Footer -->
									<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
										<tr>
											<td valign="top">
												<table border="0" cellpadding="10" cellspacing="0" width="100%">
													<tr>
														<td colspan="2" valign="middle" id="credit">
															<?php $socials = get_field('socials','options');?>
															<?php if(array_exists($socials)): ?>
																<p style="background: #efefef; display: inline-block;">
																	<?php foreach($socials as $social_key => $social): ?>
																		<a href="<?php echo $social; ?>">
																			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/<?php echo $social_key; ?>-logo.png" width="25" style="margin: 8px;">
																		</a>
																	<?php endforeach; ?>
																</p>
															<?php endif; ?>
															<?php echo wpautop( wp_kses_post( wptexturize( apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) ) ) ) ); ?>
															<p><a href="//twopluso.com/faqs"><?php _e('FAQ','woocommerce'); ?></a></p>
															<p><a href="//twopluso.com/faqs/delivery"><?php _e('When can I expect my delivery?','woocommerce'); ?></a></p>
															<p><a href="//twopluso.com/faqs/delivery"><?php _e('Can I choose a different delivery address?','woocommerce'); ?></a></p>

															<p><?php _e('Reach us','woocommerce'); ?></p>
															<a href="mailto:support@twopluso.com">support@twopluso.com</a>
															<p><a target="_blank" href="<?php _e( home_url( '/#section-subscribe' ) ); ?>"><?php _e('To subscribe to our newsletter, please click here','woocommerce'); ?></a></p>
															<p><?php _e('2+O<sup>®</sup>. an Envregy company','woocommerce'); ?></p>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- End Footer -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
