<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( is_user_logged_in() ) {
	return;
}

?>
<form class="woocommerce-form-- woocommerce-form-login-- login--" method="post" <?php echo ( $hidden ) ? 'style="display:none;"' : ''; ?>>
	<?php do_action( 'woocommerce_login_form_start' ); ?>

	<div class="gh1 cr-co-col col-top">
		<div class="sh-ca-title d-flex flex-center justify-content-center justify-content-sm-start bp-img">
			<div><i class="icon-icon-user-login icon-60"></i></div>
			<h3 class="bp-title fz-24 co-gray-1 fw-600"><?php _e('Login to your account','woocommerce'); ?></h3>
			<?php echo ( $message ) ? wpautop( wptexturize( $message ) ) : ''; // @codingStandardsIgnoreLine ?>
		</div>
		<div class="con-form-group fz-14 mt4">
			<div class="form-group">
				<input type="text" class="form-control cf-input" name="username" id="username" autocomplete="username" placeholder="Your email address" />
			</div>
			<div class="form-group">
				<input class="form-control cf-input" type="password" name="password" id="password" autocomplete="current-password" placeholder="Your Password" />
			</div>
			<?php do_action( 'woocommerce_login_form' ); ?>
			<div class="form-group">
				<label class="cf-checkbox">
					<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> 
					<span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
				</label>
			</div>
		</div>
	</div>
	<div class="cr-co-col col-bottom mt6">
		<div class="cr-co-ctrl d-flex flex-center justify-content-center justify-content-sm-start fz-14">
			<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
			<button type="submit" class="button btn-black btn-size-3 fw-600" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Login', 'woocommerce' ); ?></button>
			<input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ) ?>" />
			<a class="link fw-600" href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'I forgot my password', 'woocommerce' ); ?></a>
		</div>
	</div>
	<?php do_action( 'woocommerce_login_form_end' ); ?>
</form>
