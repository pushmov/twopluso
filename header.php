<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Two_Plus_O
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> style="margin-top:0 !important;">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Security-Policy"
  content="default-src * https://js.stripe.com;
  script-src * 'unsafe-eval' 'unsafe-inline';
  style-src 'self' 'unsafe-inline' https://maxcdn.bootstrapcdn.com https://fonts.googleapis.com;
  img-src 'self' https://js.stripe.com data: https://storage.googleapis.com https://scontent.cdninstagram.com https://www.google-analytics.com;">
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#42494E">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#42494E">

<?php wp_head(); ?>


</head>
<body <?php body_class(); ?>>
<?php
if ( function_exists( 'get_fields' ) ){
  $fields = get_fields('options');
  if($fields) extract($fields);
}
global $woocommerce;
$shipping_country = $woocommerce->customer->get_country();
?>
<section id="main-container">
	<header>
		<div class="header-content">
			<?php if(array_exists($top_header)): ?>
				<?php foreach($top_header as $t_header): ?>
					<?php if( in_array( $shipping_country , $t_header['country']) ): ?>
						<div class="container-fluid he-co-row note">
							<div class="container width-1 text-center">
								<div class="hc-ro-col in-co-par fz-14 fw-500 d-flex flex-center justify-content-center">
									<span><i class="icon-icon-delivery-truck fz-30"></i></span>
									<span><?php echo $t_header['text']; ?></span>
								</div>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach;?>
			<?php endif; ?>
			<div class="container-fluid he-co-row main">
				<div class="d-flex flex-center justify-content-between">
					<div>
						<div class="hc-ro-list d-flex flex-center">
							<div class="menu-bar"><span></span><span></span><span></span></div>
							<div class="logo bp-img">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
									<img src="<?php echo $logo['url']; ?>" width="60" alt="">
								</a>
							</div>
							<div class="bp-desktop">
								<?php 
								wp_nav_menu( array(
									    'container_class'   => "menu fw-500",
									    'theme_location'    => "top-menu"
									) );
								?>
							</div>
						</div>
					</div>
					<div>
						<ul class="hc-ro-user-opt bp-img d-flex flex-center">
							<li class="bp-mobile2">

								<div toggle-group="type1" class="container-fluid animate" anim-delay="2" anim-name="fadeInRight">
									<div toggle-set class="hs-su-dest fz-14">
										<div toggle-btn class="hs-de-btn fw-500 fr">
											<?php get_current_country_lang(); ?>
										</div>
										<div class="clr"></div>
										<div toggle-main class="hs-de-box text-left mt2">
											<span toggle-btn style="position: absolute;right: 0;top: 0;background: #42494E;padding: 10px;"><i class="icon-icon-close fz-16" style="color: #fff;"></i></span>
											<p><?php _e('Choose your shipping destination and language','two-plus-o'); ?></p>
											<ul class="hd-bo-list">
												<li class="fz-14">
													<?php get_all_country_dropdown(); ?>
												</li>
												<li class="fz-14">
													<?php get_all_languages_dropdown(); ?>
												</li>
												<!--
												<li class="fz-14 select2">
													<?php echo(do_shortcode("[woocs show_flags=0 width='100%' txt_type='desc']")); ?>
												</li>
												-->
												<li class="fz-14 select2">
													<?php echo(do_shortcode("[woocs show_flags=0 width='100%' txt_type='desc']")); ?>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<!-- <a data-fancybox href="#popup-destination" class="hs-de-btn fw-500">
									<?php get_current_country_lang(); ?>
								</a> -->
							</li>
							<li class="sub bp-desktop2">
								<div class="hr-us-icon hs-de-btn fw-500">
									<?php get_current_country_lang(); ?>
								</div>
								<div class="hr-us-sub hs-de-box text-left">
									<p><?php _e('Choose your shipping destination and language','woocommerce'); ?></p>
									<ul class="hd-bo-list">
										<li class="fz-14">
											<?php get_all_country_dropdown(); ?>
										</li>
										<li class="fz-14">
											<?php get_all_languages_dropdown(); ?>
										</li>
										<li class="fz-14 select2">
											<?php echo(do_shortcode("[woocs show_flags=0 width='100%' txt_type='desc']")); ?>
										</li>
									</ul>
								</div>
							</li>
							<li><div class="hr-us-icon"><a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>"><i class="icon-icon-user"></i></a></div></li>
							<li class="sub sub-box">
								<div class="hr-us-icon"><a><i class="icon-icon-shopping-cart"></i><div class="tag"><?php echo count(WC()->cart->cart_contents); ?></div></a></div>
								<div class="hr-us-sub">
									<div class="hu-su-box">
										<?php woocommerce_mini_cart(); ?>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="he-co-floating main">
		<div toggle-group="type3" class="hc-fl-list fz-14">
			<?php 
			wp_nav_menu( array(
				    'container'   		=> "",
				    'theme_location'    => "primary-menu",
        			'items_wrap' 		=> '%3$s',
				    'walker' 			=> new description_walker
				) );

			?>
		</div>
	</div>
	<div class="he-co-floating cover"></div>
