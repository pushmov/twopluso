<?php
/**
 * Template Name: Journal
 */

?>
<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php
    if ( function_exists( 'get_fields' ) ){
      $fields = get_fields();
      if($fields) extract($fields);
    }
	?>

	<section id="main-wrapper" class="push-top">
		<?php if(array_exists($slider)): ?>
			<div product-carousel="type2" class="container-fluid animate">
				<div class="container width-1">
					<div class="blog-slider mob-pull-img">
						<div class="owl-carousel nav-2 car-2">
							<?php foreach($slider as $slide_id): ?>
							<div class="item">
								<a class="bl-sl-set" href="<?php echo get_permalink($slide_id); ?>">
									<div class="container-fluid bs-se-col main co-white">
										<div class="container width-4">
											<div style="max-width: 450px;">
												<div class="in-co-par fz-14 animate" anim-control="parent">
													<h3 class="bp-title bp-tt fw-500 fz-26"><?php echo get_the_title($slide_id); ?></h3>
													<p><small><?php echo get_the_date('j M Y', $slide_id); ?></small></p>
													<p><?php echo get_the_excerpt($slide_id); ?></p>
												</div>
											</div>
										</div>
									</div>
									<div class="parallax top bp-ab" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($slide_id) ); ?>')"></div>
									<div class="parallax top cover"></div>
								</a>
							</div>
						<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php if(get_the_content()): ?>
			<div class="container-fluid mt5 animate">
				<div class="container width-1">
					<div class="in-co-par font-b fz-16 text-center">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php if(array_exists($featured_posts)): ?>
		<div class="container-fluid mt10 animate">
			<div class="container width-1">
				<div class="in-co-par fz-16 text-center">
					<h2 class="bp-title head-1"><?php _e('Featured Posts' , 'twopluso'); ?></h2>
				</div>
				<div class="pt2 animate" anim-control="parent">

					<?php foreach($featured_posts as $featured_key => $featured_id): ?>
						<div class="row d-flex flex-center justify-content-between mt3 <?php echo $featured_key%2==1 ? 'flex-row-reverse' : ''; ?>">
							<div class="col-sm-5 mt2 mb2">
								<div class="bp-img wide">
									<a href="<?php echo get_permalink($featured_id); ?>">
										<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($featured_id) ); ?>">
									</a>
								</div>
							</div>
							<div class="col-sm-7 mt2 mb2">
								<div class="container" style="max-width: 445px;">
									<div class="in-co-par fz-14">
										<h3 class="bp-title fz-26 bp-tt co-black fw-500"><?php echo get_the_title($featured_id); ?></h3>
										<p class="co-gray-1"><small><?php echo get_the_date('j M Y', $featured_id); ?></small></p>
										<p><?php echo get_the_excerpt($featured_id); ?></p>
										<p class="fz-16"><a class="link" href="<?php echo get_permalink($featured_id); ?>"><?php _e('Read More +' , 'twopluso'); ?></a></p>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>

				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php
		$args = array(  
			'post_status' => 'publish',
			'posts_per_page' => 9
		);
	   	$loop = new WP_Query( $args );
	   	if($loop->have_posts()): ?>
			<div class="container-fluid mt10 animate">
				<div class="container width-1">
					<div class="in-co-par fz-16 text-center">
						<h2 class="bp-title head-1"><?php _e('Show all posts' , 'twopluso'); ?></h2>
					</div>
					<div class="mt5">
						<ul group-height class="ho-insta-list col3 animate" anim-control="parent">
						   	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
								<li>
									<a class="bl-fe-set" href="<?php echo get_permalink(); ?>">
										<div class="bp-img wide">
											<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
										</div>
										<div class="bf-se-col capt">
											<div class="in-co-par fz-16 fw-500 gh1">
												<p class="co-gray-1"><small><?php echo get_the_date('j M Y'); ?></small></p>
												<p><?php the_title(); ?></p>
											</div>
										</div>
									</a>
								</li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
						</ul>
						<!-- <div class="mt4 text-center animate">
							<a class="button btn-size-1 btn-black fz-14" href="/blog"><?php _e('More','twopluso'); ?></a>
						</div> -->
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
	</section>
  <?php endwhile; ?>
<?php get_footer(); ?>