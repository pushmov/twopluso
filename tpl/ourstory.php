<?php
/**
 * Template Name: Our Story
 */

?>
<?php get_header(); ?>
<?php
if ( function_exists( 'get_fields' ) ){
    $fields = get_fields();
    if($fields) extract($fields);
}
?>
<?php if(is_realy_woocommerce_page()): ?>
<section id="main-wrapper" class="push-top bg-gray-10">
    <?php the_content(); ?>
</section>
<?php else: ?>
<?php if($top_banner): ?>
<section id="main-wrapper">
    <div class="container-fluid in-co-banner">
        <div class="container-fluid ic-ba-col main mt7 mb7 mob-mt3 mob-mb3 animate">
            <div class="container width-1">
                <h1 class="bp-title fz-50 fw-600 bp-tt"><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="parallax top bp-ab animate" style="background-image: url('<?php echo $top_banner['url']; ?>');"></div>
    </div>
    <?php

    // check if the repeater field has rows of data
    if( have_rows('alt_content') ):
    $alternator = 0;
    // loop through the rows of data
    while ( have_rows('alt_content') ) : the_row();
    ?>
    <?php
    if($alternator==0){
    ?>
    <div class="bp-desktop2" style="position:relative">
    <img class="ourstoryphotos" src="<?php the_sub_field('img'); ?>" style="left:0px;" />
    </div>
    <?php
    }else{
    ?>
    <div class="bp-desktop2" style="position:relative">
    <img class="ourstoryphotos" src="<?php the_sub_field('img'); ?>" style="right:0px;" />
    </div>
    <?php
    }
    ?>
    <div class="container" style="position:relative;">
        
        <div class="in-co-par font-b fz-16 text-center">
            <div class="row alternating_content">
                <?php
                if($alternator==0){
                ?>
                <div class="col-sm-12 bp-desktop2" style="min-height:150px;">

                </div>
                <div class="col-md-6  text-left" style="min-height:150px;">
                    
                    <img src="<?php the_sub_field('img'); ?>" class="bp-mobile2"  style="display:inline-block;margin-left:-5%;" />
                    
                </div>
                <div class="col-md-6 col-sm-12 text-justify" >
                    <div class="contents">
                        <?php the_sub_field('content'); ?>
                    </div>
                </div>
                <div class="clr clear clearfix"></div>
                <?php
                    $alternator = 1;
                }else{
                ?>
                <div class="col-sm-12 bp-desktop2" style="min-height:150px;">

                </div>
                <div class="col-md-6 text-right bp-mobile2" style="min-height:150px;">

                    <img src="<?php the_sub_field('img'); ?>" class="bp-mobile2"  style="display:inline-block;margin-right:-5%;" />

                </div>
                <div class="col-md-6 col-sm-12 text-justify" >
                    <div class="contents">
                        <?php the_sub_field('content'); ?>
                    </div>
                </div>
                <div class="col-md-6 text-right bp-desktop2" style="min-height:150px;">
                    
                    
                    
                </div>
                <div class="clr clear clearfix"></div>
                <?php
                    $alternator = 0;
                }
                ?>


            </div>
        </div>
    </div>

    <?php
    endwhile;

    else :

    // no rows found

    endif;

    ?>
    <div class="container-fluid mt9 animate">
        

        <div class="container">
            <h2 class="bp-title head-2 text-center"><?php the_field('commitment_label'); ?></h2>
            <div class="in-co-par font-b fz-16 text-center">
                <?php

                // check if the repeater field has rows of data
                if( have_rows('commitments') ):
                $alternator = 0;
                // loop through the rows of data
                while ( have_rows('commitments') ) : the_row();
                ?>
                <div class="row alternating_content">
                    <?php
                    if($alternator==0){
                    ?>
                    <div class="col-md-6 col-sm-12 text-justify" >
                        <div class="contents">
                            <?php the_sub_field('content'); ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 text-center">
                        <img src="<?php the_sub_field('img'); ?>" class="bp-desktop2" style="float:right;" />
                        <img src="<?php the_sub_field('img'); ?>" class="bp-mobile2"  style="display:inline-block;" />
                    </div>
                    <div class="clr clear clearfix"></div>
                    <?php
                        $alternator = 1;
                    }else{
                    ?>
                    <div class="col-md-6 col-sm-12 text-center">
                        <img src="<?php the_sub_field('img'); ?>" class="bp-desktop2" style="float:left;" />
                        <img src="<?php the_sub_field('img'); ?>" class="bp-mobile2"  style="display:inline-block;" />
                    </div>
                    <div class="col-md-6 col-sm-12 text-justify" >
                        <div class="contents">
                            <?php the_sub_field('content'); ?>
                        </div>
                    </div>
                    <div class="clr clear clearfix"></div>
                    <?php
                        $alternator = 0;
                    }
                    ?>


                </div>

                <?php
                endwhile;

                else :

                // no rows found

                endif;

                ?>
            </div>
        </div>
        
        <div class="container">
            <h2 class="bp-title head-2 text-center team-label"><?php the_field('team_label'); ?></h2>
            <div class="in-co-par font-b fz-16 text-center">
                <div class="row ">
                    <?php

                    // check if the repeater field has rows of data
                    if( have_rows('team') ):
                    // loop through the rows of data
                    while ( have_rows('team') ) : the_row();
                    ?>
                    <div class="col-sm-12 col-md-6 team_members">
                        <div class="photo_container">
                            <img src="<?php the_sub_field('photo') ?>"  />
                        </div>
                        <h3 class="bp-title name text-center"><?php the_sub_field('name'); ?></h3>
                        <h4 class="bp-title designation text-center"><?php the_sub_field('designation'); ?></h4>
                        <div class="text-justify background"><?php the_sub_field('background'); ?></div>
                    </div>

                    <?php
                    endwhile;

                    else :

                    // no rows found

                    endif;

                    ?>
                </div>
            </div>
        </div>
        <div class="container width-5">
            
            <div class="mt4 text-center animate">
                <a class="button btn-size-1 btn-black fz-14" href="<?php echo wc_get_page_permalink( 'shop' ); ?>"><?php _e( 'Shop Now' , 'woocommerce' ); ?></a>
            </div>
        </div>
    </div>
    <?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
</section>
<?php else: ?>
<section id="main-wrapper" class="push-top">
    <div class="container-fluid mt6 mb6 animate">
        <div class="container width-1">
            <div class="co-temp-par animate" anim-control="parent">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
    <?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
</section>
<?php endif; ?>
<?php endif; ?>
<?php get_footer(); ?>
