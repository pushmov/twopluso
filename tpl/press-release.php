<?php
/**
 * Template Name: Press release
 */

?>
<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php
    if ( function_exists( 'get_fields' ) ){
      $fields = get_fields();
      if($fields) extract($fields);
    }
	?>

	<section id="main-wrapper" class="push-top">
		<div class="container-fluid bg-gray-7 pt4 pb4 animate">
			<div class="container width-1">
				<div class="text-center">
					<h1 class="bp-title fz-50 fw-600 bp-tt"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
		<?php if(get_the_content()): ?>
			<div class="container-fluid mt5 animate">
				<div class="container width-1">
					<div class="in-co-par font-b fz-16 text-center">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="container-fluid mt5 animate">
			<div class="container width-1">
				<!-- nav for desktop -->
				<?php
				$cat_args = array(
				    'hide_empty' => false,
				    'exclude'	 => 1
				);
				$product_categories = get_terms( 'press_release_cat', $cat_args );
				?>
				<?php if( !empty($product_categories) ): ?>
				<div class="bp-desktop2">
					<ul class="pr-re-nav fw-500 text-center">
						<li class="active"><a href="press-release"><?php _e('All','woocommerce'); ?></a></li>
						<?php foreach ($product_categories as $key => $category): ?>
							<li><a href="<?php echo get_term_link($category); ?>"><?php echo $category->name; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="bp-mobile2">
					<div class="bp-select cf-select fz-16 fw-600">
						<select name="" id="">
							<option value=""><?php _e('All','woocommerce'); ?></option>
							<?php foreach ($product_categories as $key => $category): ?>
								<option value="<?php echo get_term_link($category); ?>"><?php echo $category->name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<?php endif; ?>

				<div class="mt7">
					<?php
						$args = array(  
						   'post_type' => 'press_release',
						   'post_status' => 'publish',
						   'posts_per_page' => 6
						);
					   	$loop = new WP_Query( $args );
					   	if($loop->have_posts()): ?>
						<ul group-height class="ho-insta-list col3 animate" anim-control="parent">
						   	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<li>
								<a class="bl-fe-set" href="<?php echo get_permalink(); ?>">
									<div class="bp-img wide"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>"></div>
									<div class="bf-se-col capt">
										<div class="in-co-par fz-16 fw-500 gh1">
											<p class="co-gray-1"><small><?php echo get_the_date('j m Y'); ?></small></p>
											<p><?php the_title(); ?></p>
										</div>
									</div>
								</a>
							</li>
							<?php endwhile; ?>
						</ul>
						       

						<?php wp_reset_postdata(); ?>
					<?php endif; ?>

					<!-- <div class="mt4 text-center animate">
						<a class="button btn-size-1 btn-black fz-14" href="#">More</a>
					</div> -->
				</div>
			</div>
		</div>
		<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
	</section>
  <?php endwhile; ?>
<?php get_footer(); ?>