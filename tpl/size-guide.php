<?php
/**
 * Template Name: Size Guide
 */

?>
<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php
    if ( function_exists( 'get_fields' ) ){
      $fields = get_fields();
      if($fields) extract($fields);
    }
	?>
	<section id="main-wrapper" class="push-top">
		<div class="container-fluid in-co-banner">
			<div class="container-fluid ic-ba-col main mt7 mb7 mob-mt3 mob-mb3 animate">
				<div class="container width-1">
					<h1 class="bp-title fz-50 fw-600 bp-tt"><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="parallax top bp-ab animate" style="background-image: url('<?php echo $top_banner['url']; ?>');"></div>
		</div>
		<div class="container-fluid mt6 mb6 animate">
			<div class="container width-1">
				<div class="row">
					<div class="col-sm-12">
						<div class="in-co-par fz-16 text-center">
							<p><?php the_content(); ?></p>
							<p><img src="<?php the_field('img_1') ?>" width="100%" /></p>
							<p><img src="<?php the_field('img_2') ?>" width="100%" /></p>
						</div>
					</div>
				</div>
				<?php if(array_exists($tables)): ?>
					<?php foreach($tables as $table): ?>
						<div>
							<?php if($table['title']): ?>
								<h2 class="bp-title fz-24 mt2 mb2 fw-600 text-center"><?php echo $table['title']; ?></h2>
							<?php endif; ?>
							<?php if(array_exists($table['rows'])): ?>
								<div class="mob-pull-img">
									<table class="table-sg fz-14 text-center">
										<tbody>
											<tr>
												<th><?php _e('Size','twopluso'); ?></th>
												<th><?php _e('Age','twopluso'); ?></th>
												<th><?php _e('Height (cm)','twopluso'); ?></th>
												<th><?php _e('Weight (kg)','twopluso'); ?></th>
												<th><?php _e('Bust (cm)','twopluso'); ?></th>
												<th><?php _e('Waist (cm)','twopluso'); ?></th>
												<th><?php _e('Hip (cm)','twopluso'); ?></th>
											</tr>
											<?php foreach($table['rows'] as $row): ?>
												<tr>
													<td><?php echo  $row['size']; ?></td>
													<td><?php echo  $row['age']; ?></td>
													<td><?php echo  $row['height']; ?></td>
													<td><?php echo  $row['weight']; ?></td>
													<td><?php echo  $row['bust']; ?></td>
													<td><?php echo  $row['waist']; ?></td>
													<td><?php echo  $row['hip']; ?></td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
				<div class="mt5 text-center">
					<a class="button btn-size-1 btn-black fz-14 fw-600 bp-tt" href="<?php echo wc_get_page_permalink( 'shop' ); ?>"><?php _e('Back to Product','twopluso'); ?></a>
				</div>
			</div>
		</div>
		<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
	</section>
  <?php endwhile; ?>
<?php get_footer(); ?>