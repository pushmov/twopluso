<?php
/**
 * Template Name: Homepage
 */

?>
<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php
    if ( function_exists( 'get_fields' ) ){
      $fields = get_fields();
      if($fields) extract($fields);
    }
	?>
	<section id="main-wrapper">
		<?php if(array_exists($sliders)): ?>
			<div class="home-slider container-fluid no-padding bp-rel height-full">
				<div class="flexslider">
					<ul class="slides">
						<?php foreach($sliders as $slider_key => $slider): ?>
						<li class="height-full">
							<div class="ho-sl-main bp-ab">
								<div class="bp-mobile2 bp-img text-center">
									<img src="<?php echo $slider['image_mobile']['url']; ?>">
								</div>
								<div class="parallax bp-ab bp-desktop2" data-stellar-background-ratio="0.4" style="background-image: url('<?php echo $slider['image']['url']; ?>')"></div>
								<div class="container-fluid hs-ma-col capt">
									<div class="container width-1">
										<div class="in-co-par fz-14 text-center-xs animate" anim-control="parent">
											<?php if($slider['sub_title']): ?>
												<h4 class="fz-16 fw-600 co-blue-1"><?php echo $slider['sub_title']; ?></h4>
											<?php endif; ?>
											<?php if($slider['sub_title']): ?>
												<h1 class="fz-50 fw-600 bp-tt"><?php echo $slider['title']; ?></h1>
											<?php endif; ?>
											<?php if($slider['button']['label'] && $slider['button']['url']): ?>
												<div class="mt4">
													<a class="button btn-black" href="<?php echo $slider['button']['url']; ?>"><?php echo $slider['button']['label']; ?></a>
												</div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</li>
					<?php endforeach; ?>
					</ul>
				</div>

				<div class="ho-sl-sub bottom mb5">
					<div class="d-flex flex-bottom justify-content-between">
						<div class="ho-sl-num d-flex flex-center container-fluid font-b fz-26 animate" anim-control="parent" anim-name="fadeInLeft"  anim-delay="1">
							<div class="num-first fw-600">00</div>
							<div class="line"></div>
							<div class="num-last co-gray-3">00</div>
						</div>
						<?php if($slider_arrow): ?>
							<div class="container-fluid ho-sl-nav d-flex flex-center animate" anim-control="parent" anim-name="fadeInRight" anim-delay="1">
								<div><a class="nav-prev" href="#"><i class="icon-icon-prev-left"></i></a></div>
								<div><a class="nav-next" href="#"><i class="icon-icon-next-right"></i></a></div>
							</div>
						<?php endif; ?>
					</div>
				</div>

				<div class="ho-sl-sub bottom-center mb3">
					<div class="hs-su-arrow animate" anim-delay="2"><span><img src="<?php echo get_template_directory_uri(); ?>/assets/svg/scroll-down.svg"></span></div>
				</div>

				<div class="ho-sl-sub right-center">
					<div toggle-group="type1" class="container-fluid animate" anim-delay="2" anim-name="fadeInRight">
						<div toggle-set class="toggle-active hs-su-dest fz-14">
							<div toggle-btn class="hs-de-btn fw-500 fr">
								<?php get_current_country_lang(); ?>
							</div>
							<div class="clr"></div>
							<div toggle-main class="hs-de-box text-left mt2">
								<p><?php _e('Choose your shipping destination and language','two-plus-o'); ?></p>
								<ul class="hd-bo-list">
									<li class="fz-14">
										<?php get_all_country_dropdown(); ?>
									</li>
									<li class="fz-14">
										<?php get_all_languages_dropdown(); ?>
									</li>
									<!--
									<li class="fz-14 select2">
										<?php echo(do_shortcode("[woocs show_flags=0 width='100%' txt_type='desc']")); ?>
									</li>
									-->
									<li class="fz-14 select2">
										<?php echo(do_shortcode("[woocs show_flags=0 width='100%' txt_type='desc']")); ?>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div product-carousel="type1" class="container-fluid mt10 animate">
			<div class="container width-1">
				<div class="in-co-par fz-16 text-center">
					<?php if($featured_title): ?>
						<h2 class="bp-title head-1"><?php echo $featured_title; ?></h2>
					<?php endif; ?>
					<?php if($featured_button['url']): ?>
						<p><a class="link" href="<?php echo $featured_button['url']; ?>"><?php echo $featured_button['label']; ?></a></p>
					<?php endif; ?>
				</div>

				<?php if(array_exists($featured_products)): ?>
					<div class="mt8">
						<div group-height class="owl-carousel nav-1 car-1">
							<?php foreach($featured_products as $f_product_key => $f_product): ?>
								<div class="item">
									<div class="bp-img wide">
										<a href="<?php echo get_permalink($f_product->ID); ?>">
											<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($f_product->ID) );?>" alt="">
										</a>
									</div>
									<div class="in-co-par text-center fz-14 fw-500 mt2">
										<div class="fe-pr-capt gh1">
											<?php 
											$terms = get_the_terms( $f_product->ID, 'product_cat' );
											foreach ($terms as $term): ?>
												<p class="bp-tt"><?php echo $term->name; ?></p>
												<?php break; ?>
											<?php endforeach; ?>
											<p class="fz-16 bp-tt"><?php echo $f_product->post_title; ?></p>
										</div>
										<div class="mt3">
											<a class="button btn-black" href="<?php echo get_permalink($f_product->ID); ?>"><?php _e('Shop Now','woocommerce'); ?></a>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>

		<?php if(array_exists($promotion_banners)): ?>
			<div class="container-fluid mt8 animate">
				<div class="container width-1">
					<div class="row animate" anim-control="parent">
						<?php foreach($promotion_banners as $p_banner): ?>
							<div class="col-md-6 mb3">
								<div class="bp-img wide mob-pull-img"><a href="<?php echo $p_banner['url']; ?>"><img src="<?php echo $p_banner['image']['url']; ?>" alt="<?php echo $p_banner['image']['alt']; ?>"></a></div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php if(array_exists($shop_by_categories)): ?>
			<div class="container-fluid mt8 animate">
				<div class="container width-1">
					<?php if($shop_by_categories_title): ?>
						<div class="in-co-par fz-16 text-center">
							<h2 class="bp-title head-1"><?php echo $shop_by_categories_title; ?></h2>
						</div>
					<?php endif; ?>
					<div class="mt5">
						<div class="row animate" anim-control="parent">
							<?php foreach($shop_by_categories as $category): ?>
								<div class="col-md-4 mb3">
									<div class="bp-img wide mob-pull-img">
										<a href="<?php echo $category['url']; ?>">
											<img src="<?php echo $category['thumbnail']['url']; ?>" alt="<?php echo $category['thumbnail']['alt']; ?>">
										</a>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="container-fluid mt10 animate">
			<div class="container width-1">
				<div class="in-co-par fz-16 text-center">
					<h2 class="bp-title head-1" style="text-transform:none !important;"><?php the_field('instagram_title') ?></h2>
				</div>
				<div class="mt5">
					<?php echo do_shortcode('[instagram-feed]'); ?>
				</div>
			</div>
		</div>
		<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
	</section>
  <?php endwhile; ?>
<?php get_footer(); ?>