<?php
/**
 * Template Name: Store Locator
 */

?>
<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php
    if ( function_exists( 'get_fields' ) ){
      $fields = get_fields();
      if($fields) extract($fields);
    }
	?>

	<section id="main-wrapper" class="push-top">
		<div class="container-fluid store-locator-content height-main bp-rel animate">
			<div class="st-lo-co main container-fluid animate" anim-name="fadeInLeft" anim-delay="1">
				<div class="sl-co-box">
					<div class="sc-bo-col bp-mobile2"><div class="sb-co-col arrow"></div></div>
					<div class="sc-bo-col top">
						<div class="sb-co-col title d-flex flex-center bp-img">
							<div class="icon"><i class="icon-icon-shop"></i></div>
							<div>
								<h4 class="bp-title fz-24 bp-tt fw-600 no-margin">store locator</h4>
							</div>
						</div>
						<div class="sb-co-form input-group fz-12 mt2">
							<input class="form-control" type="text" name="" value="" placeholder="Search Store">
							<div class="input-group-append">
								<button><i class="icon-icon-search"></i></button>
							</div>
						</div>
					</div>
					<div class="sc-bo-col main">
					<?php
						$args = array(  
						   'post_type' => 'store_locator',
						   'post_status' => 'publish',
						   'posts_per_page' => -1
						);
					   	$loop = new WP_Query( $args );
					   	if($loop->have_posts()): ?>
						<ul class="sc-bo-list in-co-par font-b fz-14">
							<?php $i = 1; ?>
						   	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
						   		<?php $address = get_field('address'); ?>
						   		<?php $opening_hours = get_field('opening_hours'); ?>
								<li>
									<div class="sc-bo-set d-flex">
										<div class="icon">
											<div class="sb-li-num"><?php echo $i; ?></div>
										</div>
										<div class="in-co-par">
											<h3 class="font-a"><?php the_title(); ?></h3>
											<p><?php echo $address; ?></p>
										</div>
										<div class="popup-holder" style="display: none;">
											<div class="sb-co-popup">
												<div class="sc-pop-box">
													<div class="sp-bo-col top in-co-par fz-14 bp-mobile2"><div class="sc-bo-close"><i class="icon-icon-arrow-left"></i> <span>Back to search results</span></div></div>
													<span class="arrow"></span>
													<div class="bp-img wide"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>"></div>
													<div class="sp-bo-col main">
														<div class="pt1">
															<h4 class="bp-title fz-20 fw-500"><?php the_title(); ?></h4>
														</div>
														<div class="in-co-par par-height-2 fz-14 font-b pt1">
															<div class="pt1">
																<p class="co-blue-1 no-margin">Address</p>
																<p><?php echo $address; ?></p>
															</div>
															<?php if($opening_hours): ?>
																<div class="pt1">
																	<p class="co-blue-1 no-margin">Opening Hours</p>
																	<ul class="sp-bo-list">
																		<?php foreach($opening_hours as $opening): ?>
																			<li>
																				<span><?php echo $opening['day']; ?></span>
																				<span><?php echo $opening['start']; ?> - <?php echo $opening['end']; ?></span>
																			</li>
																		<?php endforeach; ?>
																	</ul>
																</div>
															<?php endif; ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<?php $i++; ?>
							<?php endwhile; ?>
						</ul>
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
					</div>
				</div>
			</div>
			<div id="store-locator-map" class="bp-ab"></div>
			<script>
				// StreetViewPanorama
				var storeLocator = function() {
					var _map = {
						target: '',
						stage: '',
						init: function() {
							_map.defaults();
							_map.generate();
						},
						defaults: function() {
							_map.target = '#store-locator-map';
						},
						generate: function() {
							_map.stage = new google.maps.Map(
								document.querySelector(_map.target), {
								center: {lat: 1.352951, lng: 103.8605368},
								zoom: 14,
								zoomControl: true,
								fullscreenControl: false,
								mapTypeControl: false,
					       		styles: [
								  {
								    "elementType": "geometry",
								    "stylers": [
								      {
								        "color": "#f5f5f5"
								      }
								    ]
								  },
								  {
								    "elementType": "labels.icon",
								    "stylers": [
								      {
								        "visibility": "off"
								      }
								    ]
								  },
								  {
								    "elementType": "labels.text.fill",
								    "stylers": [
								      {
								        "color": "#616161"
								      }
								    ]
								  },
								  {
								    "elementType": "labels.text.stroke",
								    "stylers": [
								      {
								        "color": "#f5f5f5"
								      }
								    ]
								  },
								  {
								    "featureType": "administrative.land_parcel",
								    "elementType": "labels.text.fill",
								    "stylers": [
								      {
								        "color": "#bdbdbd"
								      }
								    ]
								  },
								  {
								    "featureType": "poi",
								    "elementType": "geometry",
								    "stylers": [
								      {
								        "color": "#eeeeee"
								      }
								    ]
								  },
								  {
								    "featureType": "poi",
								    "elementType": "labels.text.fill",
								    "stylers": [
								      {
								        "color": "#757575"
								      }
								    ]
								  },
								  {
								    "featureType": "poi.park",
								    "elementType": "geometry",
								    "stylers": [
								      {
								        "color": "#e5e5e5"
								      }
								    ]
								  },
								  {
								    "featureType": "poi.park",
								    "elementType": "labels.text.fill",
								    "stylers": [
								      {
								        "color": "#9e9e9e"
								      }
								    ]
								  },
								  {
								    "featureType": "road",
								    "elementType": "geometry",
								    "stylers": [
								      {
								        "color": "#ffffff"
								      }
								    ]
								  },
								  {
								    "featureType": "road.arterial",
								    "elementType": "labels.text.fill",
								    "stylers": [
								      {
								        "color": "#757575"
								      }
								    ]
								  },
								  {
								    "featureType": "road.highway",
								    "elementType": "geometry",
								    "stylers": [
								      {
								        "color": "#dadada"
								      }
								    ]
								  },
								  {
								    "featureType": "road.highway",
								    "elementType": "labels.text.fill",
								    "stylers": [
								      {
								        "color": "#616161"
								      }
								    ]
								  },
								  {
								    "featureType": "road.local",
								    "elementType": "labels.text.fill",
								    "stylers": [
								      {
								        "color": "#9e9e9e"
								      }
								    ]
								  },
								  {
								    "featureType": "transit.line",
								    "elementType": "geometry",
								    "stylers": [
								      {
								        "color": "#e5e5e5"
								      }
								    ]
								  },
								  {
								    "featureType": "transit.station",
								    "elementType": "geometry",
								    "stylers": [
								      {
								        "color": "#eeeeee"
								      }
								    ]
								  },
								  {
								    "featureType": "water",
								    "elementType": "geometry",
								    "stylers": [
								      {
								        "color": "#c9c9c9"
								      }
								    ]
								  },
								  {
								    "featureType": "water",
								    "elementType": "labels.text.fill",
								    "stylers": [
								      {
								        "color": "#9e9e9e"
								      }
								    ]
								  }
								]
							});
						}
					}
					_map.init();
				}
			</script>
			<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6MXddIXEtl3K9NlU2EmM55wYf1sjK57M&callback=storeLocator"></script>
		</div>
	</section>
  <?php endwhile; ?>
<?php get_footer(); ?>