<?php
/**
 * Template Name: FAQ
 */

?>
<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php
    if ( function_exists( 'get_fields' ) ){
      $fields = get_fields();
      if($fields) extract($fields);
    }
	?>
	<section id="main-wrapper" class="push-top">
		<div class="container-fluid mt6 animate">
			<div class="container width-1">
				<h1 class="bp-title fz-50 fw-600 bp-tt mob-mb1"><?php the_title(); ?></h1>
				<div class="in-co-par fz-14">
					<?php the_content(); ?>
				</div>
				<?php if(array_exists($section)): ?>
					<?php foreach($section as $sec_key => $sec): ?>
						<div class="<?php echo $sec_key==0 ? 'in-co-par fz-16 mt5 mob-mt3' : 'mt10'; ?>">
							<h4 class="bp-title fz-24 fw-500"><?php echo $sec['title']; ?></h4>
								<?php if(array_exists($sec['topics'])): ?>
									<div group-height class="row">
										<?php foreach($sec['topics'] as $topic_key => $topic): ?>
											<?php if($topic['acf_fc_layout'] == 'list'): ?>
												<div class="col-sm-12" id="list">
													<ul toggle-group="type2" class="co-fa-list mt5 mob-mt3">
														<?php foreach($topic['lists'] as $li_key => $li): ?>
															<li toggle-set>
																<?php  $uqid = $sec_key.'-'.$topic_key.'-'.$li_key; ?>
																<a toggle-btn="question<?php echo $uqid; ?>" href="#question<?php echo $uqid; ?>"><?php echo $li['topic']; ?></a>
																<div toggle-main>
																	<p><?php echo $li['answer']; ?></p>
																</div>
															</li>
														<?php endforeach; ?>
													</ul>
												</div>
											<?php elseif($topic['acf_fc_layout'] == 'ordering'): ?>
											<div class="col-sm-6" id="ordering">
												<ul class="co-fa-boxes col1 mt4 fz-16 animate" anim-control="parent">
													<li>
														<div class="cf-bo-box gh1">
															<div class="cb-bo-title d-flex flex-center justify-content-between">
																<h4 class="bp-title fz-24 fw-600 no-margin"><?php _e('Ordering','two-plus-o'); ?></h4>
																<div><i class="icon-icon-ordering"></i></div>
															</div>
															<ul toggle-group="type3" class="co-fa-list mt2">
																<?php foreach($topic['lists'] as $li_key => $li): ?>
																	<li toggle-set>
																		<?php  $uqid = $sec_key.'-'.$topic_key.'-'.$li_key; ?>
																		<a toggle-btn="question<?php echo $uqid; ?>" href="#question<?php echo $uqid; ?>"><?php echo $li['topic']; ?></a>
																		<div toggle-main>
																			<p><?php echo $li['answer']; ?></p>
																		</div>
																	</li>
																<?php endforeach; ?>
																	<li><a class="link" href="<?php echo home_url('faq/ordering'); ?>"><?php _e('View More','two-plus-o'); ?></a></li>
															</ul>
														</div>
													</li>
												</ul>
											</div>
											<?php elseif($topic['acf_fc_layout'] == 'delivery'): ?>
											<div class="col-sm-6" id="delivery">
												<ul class="co-fa-boxes col1 mt4 fz-16 animate" anim-control="parent">
													<li>
														<div class="cf-bo-box gh1">
															<div class="cb-bo-title d-flex flex-center justify-content-between">
																<h4 class="bp-title fz-24 fw-600 no-margin"><?php _e('Delivery','two-plus-o'); ?></h4>
																<div><i class="icon-icon-delivery"></i></div>
															</div>
															<ul toggle-group="type3" class="co-fa-list mt2">
																<?php foreach($topic['lists'] as $li_key => $li): ?>
																	<li toggle-set>
																		<?php  $uqid = $sec_key.'-'.$topic_key.'-'.$li_key; ?>
																		<a toggle-btn="question<?php echo $uqid; ?>" href="#question<?php echo $uqid; ?>"><?php echo $li['topic']; ?></a>
																		<div toggle-main>
																			<p><?php echo $li['answer']; ?></p>
																		</div>
																	</li>
																<?php endforeach; ?>
																	<li><a class="link" href="<?php echo home_url('faq/delivery'); ?>"><?php _e('View More','two-plus-o'); ?></a></li>
															</ul>
														</div>
													</li>
												</ul>
											</div>
											<?php elseif($topic['acf_fc_layout'] == 'returns'): ?>
											<div class="col-sm-6" id="returns">
												<ul class="co-fa-boxes col1 mt4 fz-16 animate" anim-control="parent">
													<li>
														<div class="cf-bo-box gh1">
															<div class="cb-bo-title d-flex flex-center justify-content-between">
																<h4 class="bp-title fz-24 fw-600 no-margin"><?php _e('Returns','two-plus-o'); ?></h4>
																<div><i class="icon-icon-returns"></i></div>
															</div>
															<ul toggle-group="type3" class="co-fa-list mt2">
																<?php foreach($topic['lists'] as $li_key => $li): ?>
																	<li toggle-set>
																		<?php  $uqid = $sec_key.'-'.$topic_key.'-'.$li_key; ?>
																		<a toggle-btn="question<?php echo $uqid; ?>" href="#question<?php echo $uqid; ?>"><?php echo $li['topic']; ?></a>
																		<div toggle-main>
																			<p><?php echo $li['answer']; ?></p>
																		</div>
																	</li>
																<?php endforeach; ?>
																	<li><a class="link" href="<?php echo home_url('faq/returns'); ?>"><?php _e('View More','two-plus-o'); ?></a></li>
															</ul>
														</div>
													</li>
												</ul>
											</div>
											<?php elseif($topic['acf_fc_layout'] == 'reach_us'): ?>
											<div class="col-sm-6" id="reach_us">
												<ul class="co-fa-boxes col1 mt4 fz-16 animate" anim-control="parent">
													<li>
														<div class="cf-bo-box gh1">
															<div class="cb-bo-title d-flex flex-center justify-content-between">
																<h4 class="bp-title fz-24 fw-600 no-margin"><?php _e('Reach us','two-plus-o'); ?></h4>
																<div><i class="icon-icon-support"></i></div>
															</div>
															<ul class="co-fa-list fw-700 mt2">
																<?php foreach($topic['lists'] as $li_key => $li): ?>
																	<li toggle-set>
																		<?php  $uqid = $sec_key.'-'.$topic_key.'-'.$li_key; ?>
																		<a toggle-btn="question<?php echo $uqid; ?>" href="#question<?php echo $uqid; ?>"><?php echo $li['topic']; ?></a>
																		<div toggle-main>
																			<p><?php echo $li['answer']; ?></p>
																		</div>
																	</li>
																<?php endforeach; ?>
																	<li><a class="link" href="<?php echo home_url('faq/reach'); ?>-us"><?php _e('View More','two-plus-o'); ?></a></li>
															</ul>
														</div>
													</li>
												</ul>
											</div>
											<?php elseif($topic['acf_fc_layout'] == 'payment'): ?>
											<div class="col-sm-6" id="payment">
												<ul class="co-fa-boxes col1 mt4 fz-16 animate" anim-control="parent">
													<li>
														<div class="cf-bo-box gh1">
															<div class="cb-bo-title d-flex flex-center justify-content-between">
																<h4 class="bp-title fz-24 fw-600 no-margin"><?php _e('Payment','two-plus-o'); ?></h4>
																<div><i class="icon-icon-payment"></i></div>
															</div>
															<ul toggle-group="type3" class="co-fa-list mt2">
																<?php foreach($topic['lists'] as $li_key => $li): ?>
																	<li toggle-set>
																		<?php  $uqid = $sec_key.'-'.$topic_key.'-'.$li_key; ?>
																		<a toggle-btn="question<?php echo $uqid; ?>" href="#question<?php echo $uqid; ?>"><?php echo $li['topic']; ?></a>
																		<div toggle-main>
																			<p><?php echo $li['answer']; ?></p>
																		</div>
																	</li>
																<?php endforeach; ?>
																	<li><a class="link" href="<?php echo home_url('faq/payment'); ?>"><?php _e('View More','two-plus-o'); ?></a></li>
															</ul>
														</div>
													</li>
												</ul>
											</div>
											<?php elseif($topic['acf_fc_layout'] == 'general_information'): ?>
											<div class="col-sm-6" id="general_information">
												<ul class="co-fa-boxes col1 mt4 fz-16 animate" anim-control="parent">
													<li>
														<div class="cf-bo-box gh1">
															<div class="cb-bo-title d-flex flex-center justify-content-between">
																<h4 class="bp-title fz-24 fw-600 no-margin"><?php _e('General Information','two-plus-o'); ?></h4>
																<div><i class="icon-icon-info"></i></div>
															</div>
															<ul toggle-group="type3" class="co-fa-list mt2">
																<?php foreach($topic['lists'] as $li_key => $li): ?>
																	<li toggle-set>
																		<?php  $uqid = $sec_key.'-'.$topic_key.'-'.$li_key; ?>
																		<a toggle-btn="question<?php echo $uqid; ?>" href="#question<?php echo $uqid; ?>"><?php echo $li['topic']; ?></a>
																		<div toggle-main>
																			<p><?php echo $li['answer']; ?></p>
																		</div>
																	</li>
																<?php endforeach; ?>
																	<li><a class="link" href="<?php echo home_url('faq/general'); ?>"><?php _e('View More','two-plus-o'); ?></a></li>
															</ul>
														</div>
													</li>
												</ul>
											</div>
											<?php endif; ?>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
	</section>
  <?php endwhile; ?>
<?php get_footer(); ?>