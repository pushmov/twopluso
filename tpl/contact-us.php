<?php
/**
 * Template Name: Contact us
 */

?>
<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php
    if ( function_exists( 'get_fields' ) ){
      $fields = get_fields();
      if($fields) extract($fields);
    }
	?>

	<section id="main-wrapper">
		<div class="container-fluid in-co-banner">
			<div class="container-fluid ic-ba-col main mt7 mb7 mob-mt3 mob-mb3 animate">
				<div class="container width-1">
					<h1 class="bp-title fz-50 fw-600 bp-tt"><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="parallax top bp-ab animate" style="background-image: url('<?php echo $top_banner['url']; ?>');"></div>
		</div>
		<div class="container-fluid mt5 animate">
			<div class="container width-1">
				<div class="row d-flex justify-content-between text-center-xs">
					<?php if($description): ?>
						<div class="col-sm-5 mt2 mb2">
							<div class="in-co-par fz-16 font-b">
								<p><?php echo $description; ?></p>
							</div>
						</div>
					<?php endif; ?>
					<div class="col-sm-5 mt2 mb2">
						<div class="in-co-par fz-16 font-b">
							<p><?php echo $contact_detail['description']; ?></p>
						</div>
						<div class="mt3">
							<ul class="con-inf-list fz-16 font-b bp-set">
								<?php if($contact_detail['email']): ?>
									<li>
										<span class="icon"><i class="icon-icon-email"></i></span>
										<span><a href="mailto:<?php echo $contact_detail['email']; ?>"><?php echo $contact_detail['email']; ?></a></span>
									</li>
								<?php endif; ?>
								<?php if($contact_detail['time']): ?>
									<li>
										<span class="icon"><i class="icon-icon-clock"></i></span>
										<span><?php echo $contact_detail['time']; ?></span>
									</li>
								<?php endif ;?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid mt10 animate">
			<div class="container width-6">
				<div class="con-form-group fz-14">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
	</section>
  <?php endwhile; ?>
<?php get_footer(); ?>