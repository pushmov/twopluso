<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Two_Plus_O
 */

?>
<?php
if ( function_exists( 'get_fields' ) ){
  $fields = get_fields('options');
  if($fields) extract($fields);
}
?>
</section>

<!-- popup content -->
<div style="display: none;">
	<?php if($popup_enable && !isset($_SESSION['hide_popup'])): ?>
		<?php $_SESSION['hide_popup'] = 1; ?>
		<div <?php echo is_user_logged_in() ? '':'data-popup="auto-popup"'; ?> id="newsletter-popup" class="popup-content no-padding pc-width-1">
			<div class="ne-po-row">
				<?php if(array_exists($popup_image)): ?>
					<div class="ne-po-col banner bp-desktop2" style="background-image: url('<?php echo $popup_image['url']; ?>');"></div>
				<?php endif; ?>
				<?php if(array_exists($popup_image_mobile)): ?>
					<div class="ne-po-col bp-mobile2">
						<div class="bp-img wide"><img src="<?php echo $popup_image_mobile['url']; ?>" alt=""></div>
					</div>
				<?php endif; ?>
				<div class="ne-po-col main">
					<div class="np-co-col top">
						<h3 class="bp-title fz-30 fw-600 bp-tt"><?php echo $popup_title; ?></h3>
						<div class="in-co-par fz-14 mt4 mb3">
							<p><?php echo $popup_description; ?></p>
						</div>
					</div>
					<?php //echo do_shortcode('[mc4wp_form id="500"]'); ?>
					<?php gravity_form(5, false, false, false, '', true, 12); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div id="popup-destination" class="popup-content">
		<div class="in-co-par fz-14">
			<p><?php _e('Choose your shipping destination and language','two-plus-o'); ?></p>
			<ul class="hd-bo-list">
				<li>
					<?php get_all_country_dropdown(); ?>
				</li>
				<li>
					<?php get_all_languages_dropdown(); ?>
				</li>
				<li class="fz-14 select2">
					<?php echo(do_shortcode("[woocs show_flags=0 width='100%' txt_type='desc']")); ?>
				</li>
			</ul>
		</div>
	</div>
</div>

<footer>
	<div class="footer-content container-fluid pt7 pb7">
		<div class="container width-2 font-b fz-16">
			<?php if($footer_menu): ?>
				<div class="row">
					<?php foreach($footer_menu as $f_menu): ?>
						<div class="col-sm-4 mb5">
							<h4 class="fc-title"><?php echo $f_menu['title']; ?></h4>
							<div class="fc-main mt4">
								<?php if($f_menu['menu_type'] == 'links'): ?>
								<ul class="fc-links">
									<?php foreach($f_menu['links'] as $link): ?>
										<li>
											<a href="<?php echo $link['url']; ?>"><?php echo $link['label']; ?></a>
										</li>
									<?php endforeach; ?>
								</ul>
								<?php else: ?>
									<a class="button btn-size-1 btn-wide btn-black bp-tt" href="<?php echo $f_menu['button']['url']; ?>">
										<i class="icon-icon-store"></i> <span><?php echo $f_menu['button']['label']; ?></span>
									</a>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			<div class="mt5 fz-12 text-center">
				<?php if($socials['facebook'] || $socials['instagram'] || $socials['pinterest']): ?>
					<ul class="fc-sns d-flex flex-center justify-content-center bp-img wide">
						<?php if($socials['facebook']): ?>
							<li><a class="sns-fb" href="<?php echo $socials['facebook']; ?>" target="_blank"><i class="icon-icon-facebook-circle"></i></a></li>
						<?php endif; ?>
						<?php if($socials['instagram']): ?>
							<li><a class="sns-ig" href="<?php echo $socials['instagram']; ?>" target="_blank"><i class="icon-icon-instagram-circle"></i></a></li>
						<?php endif; ?>
						<?php if($socials['pinterest']): ?>
							<li><a class="sns-pinterest" href="<?php echo $socials['pinterest']; ?>" target="_blank"><i class="icon-icon-pinterest-circle"></i></a></li>
						<?php endif; ?>
					</ul>
				<?php endif; ?>
				<div class="mt2"><?php echo date('Y'); ?> © Copyright twopluso<sup>®</sup>. All Rights Reserved.</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>