<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Two_Plus_O
 */

?>
<?php
if ( function_exists( 'get_fields' ) ){
  $fields = get_fields();
  if($fields) extract($fields);
}
?>
<?php if(is_realy_woocommerce_page()): ?>
	<section id="main-wrapper" class="push-top bg-gray-10">
		<?php the_content(); ?>
	</section>
<?php else: ?>
	<?php if($top_banner): ?>
		<section id="main-wrapper">
			<div class="container-fluid in-co-banner">
				<div class="container-fluid ic-ba-col main mt7 mb7 mob-mt3 mob-mb3 animate">
					<div class="container width-1">
						<h1 class="bp-title fz-50 fw-600 bp-tt"><?php the_title(); ?></h1>
					</div>
				</div>
				<div class="parallax top bp-ab animate" style="background-image: url('<?php echo $top_banner['url']; ?>');"></div>
			</div>
			<div class="container-fluid mt9 animate">
				<div class="container width-5">
					<div class="in-co-par font-b fz-16 text-center">
						<?php the_content(); ?>
					</div>
					<div class="mt4 text-center animate">
						<a class="button btn-size-1 btn-black fz-14" href="<?php echo wc_get_page_permalink( 'shop' ); ?>"><?php _e( 'Shop Now' , 'woocommerce' ); ?></a>
					</div>
				</div>
			</div>
			<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
		</section>
	<?php else: ?>
		<section id="main-wrapper" class="push-top">
			<div class="container-fluid mt6 mb6 animate">
				<div class="container width-1">
					<div class="co-temp-par animate" anim-control="parent">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
		</section>
	<?php endif; ?>
<?php endif; ?>
