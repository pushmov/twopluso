<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Two_Plus_O
 */

?>

<section id="main-wrapper" class="push-top">
	<div class="container-fluid in-co-top animate">
		<div class="container width-1">
			<a class="ic-to-btn bp-set" href="/journal"><i class="icon-icon-arrow-left"></i> <span><?php _e('journal','two-plus-o'); ?></span></a>
			<ul class="ic-to-breadcrumbs mt2">
				<li><a href="/"><?php _e('Home','two-plus-o'); ?></a></li>
				<li><a href="/journal"><?php _e('Journal','two-plus-o'); ?></a></li>
				<li><?php the_title();?></li>
			</ul>
		</div>
	</div>
	<div class="container-fluid mt6 animate">
		<div class="container width-2">
			<div class="bl-de-row d-block d-md-flex">
				<div class="bl-de-col left">
					<div sticky-content>
						<!-- <ul class="bl-de-sns text-center mt3 mob-mb2">
							<li><a class="sns-fb" href="#"><i class="icon-icon-facebook"></i></a></li>
							<li><a class="sns-twitter" href="#"><i class="icon-icon-twitter"></i></a></li>
							<li><a class="sns-linkedin" href="#"><i class="icon-icon-linkedin"></i></a></li>
						</ul> -->
						<?php echo do_shortcode('[ss_social_share size="small" align="center" spacing="1" hide_on_mobile="0" total="0" all_networks="0"]'); ?>
					</div>
				</div>
				<div class="bl-de-col">
					<div class="in-co-par fz-14">
						<p class="co-gray-1"><small><?php echo get_the_date('j m Y'); ?></small></p>
						<h3 class="bp-title fz-26 fw-500 bp-tt co-black"><?php the_title(); ?></h3>
					</div>
					<hr class="mt5 mb5">
					<div class="in-co-par font-b fz-16 text-center">
						<?php the_content(); ?>
					</div>
					<style type="text/css">
						.in-co-par img {max-width: 100% !important;height: auto;}
					</style>
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
</section>
