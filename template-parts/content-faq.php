<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Two_Plus_O
 */

?>

<?php
if ( function_exists( 'get_fields' ) ){
  $fields = get_fields();
  if($fields) extract($fields);
}
?>

<section id="main-wrapper" class="push-top">
	<div class="container-fluid in-co-top animate">
		<div class="container width-1">
			<a class="ic-to-btn bp-set" href="/faqs"><i class="icon-icon-arrow-left"></i> <span>FAQ</span></a>
			<ul class="ic-to-breadcrumbs mt2">
				<li><a href="/">Home</a></li>
				<li><a href="/faqs">FAQ</a></li>
				<li><?php the_title();?></li>
			</ul>
		</div>
	</div>
	<div class="container-fluid mt6 animate">
		<div class="container width-2">
			<div class="bl-de-row d-block d-md-flex">
				<div class="bl-de-col">
					<!-- <div class="in-co-par fz-14">
						<h3 class="bp-title fz-26 fw-500 bp-tt co-black"><?php the_title(); ?></h3>
					</div> -->
					<!-- <hr class="mt5 mb5"> -->
					<div class="in-co-par font-b fz-16">
						<?php the_content(); ?>
						<ul toggle-group="type2" class="co-fa-list mob-mt3">
							<?php foreach($topics as $li_key => $li): ?>
								<li toggle-set>
									<a toggle-btn="question<?php echo $li_key; ?>" href="#question<?php echo $uqid; ?>"><?php echo $li['question']; ?></a>
									<div toggle-main>
										<p><?php echo $li['answer']; ?></p>
									</div>
								</li>
							<?php endforeach; ?>
						</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part( 'template-parts/content', 'subscribe' ); ?>
</section>
