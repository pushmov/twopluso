<?php
if ( function_exists( 'get_fields' ) ){
  $fields = get_fields('options');
  if($fields) extract($fields);
}
?>
<div class="container-fluid co-ne-main mb10 animate" style="position: relative;">
	<span id="section-subscribe" style="position: absolute;top: -50px;"></span>
	<div class="container width-1 bp-rel">
		<div class="container width-3 banner-main mt5 pt5 pb5">
			<div class="in-co-par fz-16 text-center">
				<h2 class="bp-title head-2"><?php echo $popup_title; ?></h2>
				<div class="mt3 co-gray-3">
					<p><?php echo $popup_description; ?></p>
				</div>
			</div>
		</div>
		<div class="parallax top bp-ab bp-desktop" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/bg-newsletter-desktop.png'); background-size: contain;"></div>
		<div class="parallax top bp-ab bp-mobile" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/bg-newsletter-mobile.png'); background-size: contain;"></div>
	</div>
	<div class="container width-3 text-center">
		<?php gravity_form(3, false, false, false, '', true, 19); ?>
		<!-- <div class="ho-co-input">
			<div class="input-group">
				<input class="form-control" type="text" name="" value="" placeholder="Enter your email address">
				<div class="input-group-append">
					<button class="button hc-btn">Submit</button>
				</div>
			</div>
		</div> -->
	</div>
</div>